from elixir import *

metadata.bind = "sqlite:///pelitajaya1.sqlite"
#metadata.bind.echo = True
session.autoflush = False

class Person(Entity):
    using_options(inheritance="multi")
    id = Field(String, primary_key=True)
    nama = Field(String)    

class Supplier(Person):    
    using_options(inheritance="multi")
    alamat = Field(String)
    kota = Field(String)
    limitKredit = Field(Integer)
    
    pembelian = OneToMany('BeliHeader')
    
    def __repr__(self):
        return "<Supplier('%s', '%s')>" % (self.id, self.nama)
    
    def display(self):
        return "%s | %s" % (self.id, self.nama)
        
class Customer(Person):
    using_options(inheritance="multi")
    alamat = Field(String)
    kota = Field(String)
    limitKredit = Field(Integer)
    
    hargaJual = OneToMany('HargaJual')
    penjualan = OneToMany('JualHeader')
    
    def __repr__(self):
        return "<Customer('%s', '%s')>" % (self.id, self.nama)
    
    def display(self):
        return "%s | %s" % (self.id, self.nama)

class Sales(Person):
    using_options(inheritance="multi")
    
    penjualan = OneToMany('JualHeader')
    
    def __repr__(self):
        return "<Sales('%s', '%s')>" % (self.id, self.nama)
    
    def display(self):
        return "%s | %s" % (self.id, self.nama)

class Barang(Entity):
    id = Field(String, primary_key=True)
    nama = Field(String)
    hargaModal = Field(Integer)
    hargaJual = Field(Integer)
    #satuan = Field(String)
    satuan = ManyToOne('Satuan')
    isi = Field(String)    
    
    daftarHarga = OneToMany('HargaJual')
    stock = OneToMany('Stock')    
    pembelian = OneToMany('BeliDetail')
    penjualan = OneToMany('JualDetail')
    
    returPembelian = OneToMany('ReturPembelianDetail')
    returPenjualan = OneToMany('ReturPenjualanDetail')
    
    def namaSatuan(self):
        return self.satuan.nama
    
    def jumlahStock(self):
        jumlah = 0
        for s in self.stock:
            jumlah = jumlah + s.qty
        return jumlah
    
    def __repr__(self):
        return "<Barang('%s', '%s')>" % (self.id, self.nama)
    
    def display(self):
        return "%s | %s" % (self.id, self.nama)

class HargaJual(Entity):
    barang = ManyToOne('Barang')
    customer = ManyToOne('Customer')
    harga = Field(Integer)
    
    def namaBarang(self):
        return self.barang.display()
    
    def __repr__(self):
        return "<HargaJual('%s', '%s', %d)>" % (self.barang, self.customer, self.harga)

class Stock(Entity):
    barang = ManyToOne('Barang')
    tanggal = Field(Date)
    hargaModal = Field(Integer)
    qty = Field(Integer)
    
    faktur = ManyToOne('Faktur')
    
    def __repr__(self):
        return "<Stock('%s', %d, %d)>" % (self.barang, self.hargaModal, self.qty)
       
class Faktur(Entity):
    using_options(inheritance="multi")
    tanggal = Field(Date)
    
    stock = OneToMany('Stock')   
        
class BeliHeader(Faktur):
    using_options(inheritance="multi")
    supplier = ManyToOne('Supplier')
    
    details = OneToMany('BeliDetail')
    
    def __repr__(self):
        return "<BeliHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.supplier)
    
    def display(self):
        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.supplier)

class BeliDetail(Entity):
    header = ManyToOne('BeliHeader')
    barang = ManyToOne('Barang')
    harga = Field(Integer)
    qty = Field(Integer)
    
    def namaBarang(self):
        return self.barang.display()
    
    def subTotal(self):
        return self.harga * self.qty
    
    def __repr__(self):
        return "<BeliDetail('%s', '%s', %d, %d)>" % (self.header, self.barang, self.harga, self.qty)
        
class JualHeader(Faktur):
    using_options(inheritance="multi")
    customer = ManyToOne('Customer')
    sales = ManyToOne('Sales')
    
    details = OneToMany('JualDetail')
    
    def __repr__(self):
        return "<JualHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.customer.nama)
    
    def display(self):
        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.customer.nama)
    
class JualDetail(Entity):
    header = ManyToOne('JualHeader', primary_key=True)
    barang = ManyToOne('Barang', primary_key=True)
    hargaModal = Field(Integer, primary_key=True)
    hargaJual = Field(Integer, primary_key=True)
    disc = Field(Integer)
    qty = Field(Integer)
    #details = OneToMany('BarangJualDetail')
    
    def namaBarang(self):
        return self.barang.display()
    
    def jumlah(self):
        jumlah = 0
        for item in self.details:
            jumlah = jumlah + item.qty
        return jumlah            
    
    def __repr__(self):
        return "<JualDetail('%s', '%s', %d, %d)>" % (self.header, self.barang.nama, 
                                                     self.hargaJual, self.qty)

#class BarangJualDetail(Entity):
#    detail = ManyToOne('JualDetail')
#    hargaBeli = Field(Integer)
#    qty = Field(Integer)
#    
#    def __repr__(self):
#        return "<BarangJualDetail('%s', %d, %d)>" % (self.detail, self.hargaBeli, self.qty)
    
    
class ReturPembelianHeader(Faktur):
    using_options(inheritance="multi")
    supplier = ManyToOne('Supplier')
    details = OneToMany('ReturPembelianDetail')
    
    def __repr__(self):
        return "<ReturPembelianHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.supplier.nama)
    
    def display(self):
        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.supplier.nama)

class ReturPembelianDetail(Entity):
    header = ManyToOne('ReturPembelianHeader')
    #barang = ManyToOne('Stock')
    barang = ManyToOne('Barang')
    harga = Field(Integer)
    qty = Field(Integer)
    
    def __repr__(self):
        return "<ReturPembelianDetail('%s', '%s', %d, %d>" % (self.header, self.barang.nama, self.harga, self.qty)
    
class ReturPenjualanHeader(Faktur):
    using_options(inheritance="multi")
    customer = ManyToOne('Customer')
    
    details = OneToMany('ReturPenjualanDetail')
    
    def __repr__(self):
        return "<ReturPenjualanHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.supplier.nama)
    
    def display(self):
        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.supplier.nama)
    
class ReturPenjualanDetail(Entity):
    header = ManyToOne('ReturPenjualanHeader')
    #barang = ManyToOne('Stock')
    barang = ManyToOne('Barang')
    harga = Field(Integer)
    qty = Field(Integer)

    def __repr__(self):
        return "<ReturPenjualanDetail('%s', '%s', %d, %d>" % (self.header, self.barang, self.harga, self.qty)
    
class Satuan(Entity):
    nama = Field(String, primary_key=True)
    barang = OneToMany('Barang')
    
    def __repr__(self):
        return "<Satuan('%s')>" % self.nama

    def display(self):
        return self.nama
    
class User(Entity):
    user = Field(String, primary_key=True)
    password = Field(String)
    
    def __repr__(self):
        return "<User('%s')>" % self.user
    
    def display(self):
        return "%s" % self.user
    
class ACL(Entity):
    user = ManyToOne('User')
    module = Field(String)
    permissions = Field(String)
    
    def __repr__(self):
        return "<ACL('%s', '%s', '%s')>" % (self.user, self.module, self.permissions)
    
    def display(self):
        return "%s | %s | %s" % (self.user, self.module, self.permissions)
    
setup_all(True)
create_all()

#raw_input()