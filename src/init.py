'''
Created on 13 Mar 2010

@author: David
'''

from db import pelitajaya as pj
from hashlib import sha1

#u = pj.User("DAVID")
u = pj.User()
u.user = 'DAVID'
u.password = sha1('testing').hexdigest()
u.active = True

#u = pj.User("PELITAJAYA")
u = pj.User()
u.user = 'PELITAJAYA'
u.password = sha1('testing').hexdigest()
u.active = True

# actions
actions = ("TAMBAH", "LIHAT", "HAPUS", "UBAH", "CETAK")
for action in actions:
    a = pj.Action()
    a.nama = action

# resources
resources = (("DATABARANG", "Data Barang"), ("DATACUSTOMER", "Data Customer"), 
             ("DATASUPPLIER", "Data Supplier"), ("DATASALES", "Data Sales"),
             ("DATAHARGAJUAL", "Data Harga Jual"), ("DATASATUAN", "Data Satuan"), 
             ("DATABANK", "Data Bank"), ("DATAPENYESUAIANSTOCK", "Data Penyesuaian Stock"),
             ("SUPPLIERPEMBELIAN", "Pembelian"), ("SUPPLIERRETURPEMBELIAN", "Retur Pembelian"), 
             ("SUPPLIERPEMBAYARAN", "Pembayaran Supplier"),
             ("CUSTOMERPENJUALAN", "Penjualan"), ("CUSTOMERRETURPENJUALAN", "Retur Penjualan"), 
             ("CUSTOMERPEMBAYARAN", "Pembayaran Customer"),
             ("BANKTRANSAKSI", "Transaksi Bank"), ("BANKREMINDERGIRO", "Reminder Giro"), 
             ("BANKPROSESPEMBAYARANGIRO", "Proses Pembayaran Giro"),
             ("BANKGIRO", "Giro"), ("USERADMINISTRASIPENGGUNA", "Administrasi Pengguna"), 
             ("USERUBAHPASSWORD", "Ubah Password"),
             ("LAPORANSTOCK", "Laporan Stock"), ("LAPORANPENJUALAN", "Laporan Penjualan"), 
             ("LAPORANPEMBELIAN", "Laporan Pembelian"),
             ("LAPORANRETURPENJUALAN", "Laporan Retur Penjualan"), 
             ("LAPORANRETURPEMBELIAN", "Laporan Retur Pembelian"),
             ("LAPORANPEMBAYARANSUPPLIER", "Laporan Pembayaran Supplier"), 
             ("LAPORANPEMBAYARANCUSTOMER", "Laporan Pembayaran Customer"),
             ("PEMBAYARANGIROSUPPLIER", "Pembayaran Giro Supplier"),
             ("PEMBAYARANGIROCUSTOMER", "Pembayaran Giro Customer"))

for resource in resources:
    r = pj.Resource()
    r.module = resource[0]
    r.nama = resource[1]

pj.session.commit()

# acls
user = pj.User.query.filter(pj.User.user == "DAVID").one()
#acl.resource = pj.Resource.query.filter(pj.Resource.module == "DATABARANG").one()
#acl.action = pj.Action.query.filter(pj.Action.nama == "TAMBAH").one()
for resource in pj.Resource.query.all():
    for action in pj.Action.query.all():
        acl = pj.ACL()
        acl.user = user
        acl.resource = resource
        acl.action = action
pj.session.commit()
                

s = pj.Satuan()
s.nama = "BAL"

s = pj.Satuan()
s.nama = "KOTAK"

c = pj.Customer()
c.kode = "0001"
c.nama = "BUDI"
c.alamat = "JL. ASIA"
c.noTelp = "081223424"
c.kota = "MEDAN"

c = pj.Customer()
c.kode = "0002"
c.nama = "AGUS"
c.alamat = "JL. ASIA"
c.noTelp = "081223424"
c.kota = "MEDAN"

pj.session.commit()

s = pj.Supplier()
s.kode = "0001"
s.nama = "IWAN"
s.alamat = "JL. ASIA"
s.noTelp = "081223424"
s.kota = "MEDAN"
s.tempoKredit = 14

s = pj.Supplier()
s.kode = "0002"
s.nama = "HENDRI"
s.alamat = "JL. ASIA"
s.noTelp = "081223424"
s.kota = "MEDAN"
s.tempoKredit = 14

s = pj.Sales()
s.kode = "0001"
s.nama = "ALI"

s = pj.Sales()
s.kode = "0002"
s.nama = "ANDI"

pj.session.commit()

b = pj.Barang()
b.kode = "0001"
b.nama = "BARANG 01"
b.isi = "10x10"
b.hargaJual = 10000
b.hargaModal = 0
b.satuan = pj.Satuan.query.filter_by(nama="BAL").one()

b = pj.Barang()
b.kode = "0002"
b.nama = "BARANG 02"
b.isi = "20x10"
b.hargaJual = 25000
b.hargaModal = 0
b.satuan = pj.Satuan.query.filter_by(nama="KOTAK").one()

b = pj.Bank()
b.noRekening = "0001"
b.nama = "BCA"

b = pj.Bank()
b.noRekening = "0002"
b.nama = "BII"

b = pj.Bank()
b.noRekening = ""
b.nama = "KAS"

pj.session.commit()
