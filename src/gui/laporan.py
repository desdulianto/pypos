'''
Created on Jan 7, 2012

@author: david
'''
import wx
from wx.lib.masked import NumCtrl
import gui
from ObjectListView import ColumnDefn
from util import populateComboBox
from db import pelitajaya as pj
from util import dateFormatString, integerFormatString, wxDateToPyDate
from ObjectListView import ObjectListView
from util import dateFormatString, integerFormatString, wxDateToPyDate
from sqlalchemy import and_
from ObjectListView import GroupListView
import browse

def getBarang(b):
    try:
        barang = pj.Barang.query.filter_by(kode=b).one()
    except:
        barang = None
    return barang


jenis = {pj.BeliDetail: 'Pembelian',
         pj.BeliHeader: 'Pembelian',
         pj.JualDetail: 'Penjualan',
         pj.JualHeader: 'Penjualan',
         pj.ReturPembelianDetail: 'Retur Pembelian',
         pj.ReturPembelianHeader: 'Retur Pembelian',
         pj.ReturPenjualanDetail: 'Retur Penjualan',
         pj.ReturPenjualanHeader: 'Retur Penjualan',
         pj.PenyesuaianStockDetail: 'Penyesuaian Stock',
         pj.PenyesuaianStockHeader: 'Penyesuaian Stock'
         }

def db_to_wxDateTime(d):
    day=d.day
    month=d.month-1
    year=d.year
    return wx.DateTimeFromDMY(day=day, month=month, year=year) 

class FrameLaporanTransaksiStock(wx.aui.AuiMDIChildFrame):
    __nama__ = 'LAPORANTRANSAKSISTOCK'
    
    def __init__( self, parent ):
        wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, 
                                            title = u"Laporan Transaksi Stock", 
                                            pos = wx.DefaultPosition, 
                                            size = wx.Size( 500,300 ), 
                                            style = wx.DEFAULT_FRAME_STYLE )

        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                              wx.TAB_TRAVERSAL)
            
        panelSizer = wx.BoxSizer(wx.VERTICAL)
        
        horSizer = wx.BoxSizer(wx.HORIZONTAL)
        # barang        
        self.staticBarang = wx.StaticText(self.panel, wx.ID_ANY, 'Barang',
                                          wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticBarang.Wrap(-1)
        horSizer.Add(self.staticBarang, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        
        self.textKodeBarang = wx.TextCtrl(self.panel, wx.ID_ANY, wx.EmptyString,
                                          wx.DefaultPosition, wx.DefaultSize,
                                          wx.TE_PROCESS_ENTER)
        horSizer.Add(self.textKodeBarang, 1, wx.ALL|wx.EXPAND, 5)
        
        self.staticNamaBarang = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString,
                                              wx.DefaultPosition, wx.DefaultSize,
                                              0)
        self.staticNamaBarang.Wrap(-1)
        horSizer.Add(self.staticNamaBarang, 2, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
        
        self.staticMulai = wx.StaticText(self.panel, wx.ID_ANY, 'Dari',
                                              wx.DefaultPosition, wx.DefaultSize,
                                              0)
        self.staticMulai.Wrap(-1)
        horSizer.Add(self.staticMulai, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
        
        self.pickerMulai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, 
                                            wx.DefaultDateTime, 
                                            wx.DefaultPosition, 
                                            wx.DefaultSize, 
                                            wx.DP_DROPDOWN )
        horSizer.Add( self.pickerMulai, 1, wx.ALL|wx.EXPAND, 5 )
        

        self.staticAkhir = wx.StaticText(self.panel, wx.ID_ANY, 'Sampai',
                                              wx.DefaultPosition, wx.DefaultSize,
                                              0)
        self.staticAkhir.Wrap(-1)
        horSizer.Add(self.staticAkhir, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)

        self.pickerAkhir = wx.DatePickerCtrl( self.panel, wx.ID_ANY, 
                                            wx.DefaultDateTime, 
                                            wx.DefaultPosition, 
                                            wx.DefaultSize, 
                                            wx.DP_DROPDOWN )
        horSizer.Add( self.pickerAkhir, 1, wx.ALL|wx.EXPAND, 5 )


        
        panelSizer.Add(horSizer, 0, wx.ALL|wx.EXPAND, 5)
        
        # object list
        self.objListDetail = GroupListView(self.panel, wx.ID_ANY, 
                                           style=wx.LC_REPORT|wx.SUNKEN_BORDER,
                                           sortable=False)
#        self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        panelSizer.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )

        self.panel.SetSizer( panelSizer )
        self.panel.Layout()
        panelSizer.Fit( self.panel )
        sizer.Add( self.panel, 1, wx.EXPAND, 5 )
        
        self.SetSizer(sizer)
        self.Layout()            

        self.objListDetail.SetColumns([
            ColumnDefn('Tanggal', 'left', 150, 'tanggal', stringConverter=dateFormatString),
#            ColumnDefn('Tanggal', 'left', 100, 'tanggal', stringConverter=dateFormatString),                        
            ColumnDefn("Barang", "left", 300, "barang"),
            ColumnDefn("Jenis Transaksi", "left", 120, 'jenis'),
            ColumnDefn('No Faktur', 'left', 150, 'noFaktur'),
            ColumnDefn("Qty", "right", 80, "qty", stringConverter=integerFormatString),
            ColumnDefn("Saldo", "right", 100, "saldo", stringConverter=integerFormatString),
            ColumnDefn("User", "left", 150, 'oleh')
        ])
        self.objListDetail.SetAlwaysGroupByColumn(2)
        self.objListDetail.SetEmptyListMsg("Tidak ada data")

        self.populateList()
        self.textKodeBarang.SetFocus()
        
        # bind event
        self.textKodeBarang.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.textKodeBarang.Bind(wx.EVT_TEXT_ENTER, self.OnKodeBarangEnter)
        self.objListDetail.Bind(wx.EVT_LEFT_DCLICK, self.OnSelectItem)        
        
        
        self.pickerAkhir.Bind(wx.EVT_DATE_CHANGED, self.OnTanggalPicker)
        self.pickerMulai.Bind(wx.EVT_DATE_CHANGED, self.OnTanggalPicker)
        
    def OnKeyDown(self, event):
        handlers = {self.textKodeBarang: self.OnKodeBarangKeyDown}
        obj = event.GetEventObject()
        try:
            handlers[obj](event)
        except:
            event.Skip()
            
    def OnKodeBarangKeyDown(self, event):
        self.textKodeBarang.SetBackgroundColour('#ffffff')
        keyCode = event.GetKeyCode()
        if keyCode == wx.WXK_F5:
            brw = browse.DialogBrowseBarang(self)
            brw.ShowModal()
            brw.Destroy()
            if brw.selected is not None and not brw.escape:
                self.textKodeBarang.SetValue(brw.selected.kode)
        else:
#            barang = getBarang(self.textKodeBarang.GetValue().strip().upper())
#            if barang is not None:
#                self.staticNamaBarang.SetLabel(barang.nama)
#                self.populateObjListDetail()
#            else:
#                self.staticNamaBarang.SetLabel('')
#                if self.textKodeBarang.GetValue().strip() == '':
#                    self.populateObjListDetail()
#                else:
#                    self.textKodeBarang.SetBackgroundColour('#ffff00')
            event.Skip()
            
    def OnKodeBarangEnter(self, event):
        barang = getBarang(self.textKodeBarang.GetValue().strip().upper())
        if barang is not None:
            self.staticNamaBarang.SetLabel(barang.nama)
        else:
            self.staticNamaBarang.SetLabel('')
        self.populateList()
        event.Skip()
                    

    def barangGroup(self, item):
        return item['barang']

    def populateList(self):
        # query transaksi
        trans = []
        
        kodeBarang = '%s%s%s' % ('%', self.textKodeBarang.GetValue().strip().upper(), '%')
        mulai = wxDateToPyDate(self.pickerMulai.GetValue()) 
        akhir = wxDateToPyDate(self.pickerAkhir.GetValue())

        query = pj.Barang.query.filter(pj.Barang.kode.like(kodeBarang)).all()
        for barang in query:
            saldo = 0
            subQuery = pj.Transaksi.query.filter(and_(pj.Transaksi.barang == barang,
                                                      pj.Transaksi.posted == False))
            subQuery = subQuery.join(pj.Faktur).filter(and_(pj.Faktur.tanggal >= mulai,
                                                            pj.Faktur.tanggal <= akhir)) 
            for transaksi in subQuery.all():                
                if (isinstance(transaksi, pj.BeliDetail) or 
                    isinstance(transaksi, pj.ReturPenjualanDetail) or
                    isinstance(transaksi, pj.PenyesuaianStockDetail)):
                    saldo = saldo + transaksi.qty
                else:
                    saldo = saldo - transaksi.qty
#                print transaksi.header.tanggal, transaksi.qty, saldo
                item = {}
                item['tanggal'] = transaksi.header.tanggal                
                item['jenis'] = jenis[transaksi.__class__]
                item['noFaktur'] = transaksi.header.noFaktur
                item['barang'] = barang
                item['qty'] = transaksi.qty
                item['saldo'] = saldo
                item['oleh'] = transaksi.header.createdBy.user
                item['obj'] = transaksi
                
                trans.append(item)
        self.objListDetail.SetObjects(trans)

    def OnTanggalPicker(self, event):
        self.populateList()
        
    def OnSelectItem(self, event):
        self.selected = self.objListDetail.GetSelectedObject()
        frm = FrameLaporanTransaksi(self.GetTopLevelParent(), self.selected['obj'].header,
                                    jenis=jenis[self.selected['obj'].__class__])
        frm.Show()
        event.Skip()


class FrameLaporanTransaksi(wx.aui.AuiMDIChildFrame):
    __nama__ = 'LAPORANTRANSAKSI'
    
    def __init__(self, parent, header, jenis=''):
        wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, 
                                        title = u"Detail Transaksi Stock", 
                                        pos = wx.DefaultPosition, 
                                        size = wx.Size( 500,300 ), 
                                        style = wx.DEFAULT_FRAME_STYLE )
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                              wx.TAB_TRAVERSAL)
            
        panelSizer = wx.BoxSizer(wx.VERTICAL)
        
        horSizer = wx.BoxSizer(wx.HORIZONTAL)

        # tanggal
        self.staticTanggal = wx.StaticText(self.panel, wx.ID_ANY,
                                          'Tanggal', wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        self.staticTanggal.Wrap(-1)
        horSizer.Add(self.staticTanggal, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
        self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, 
                                            wx.DefaultDateTime, 
                                            wx.DefaultPosition, 
                                            wx.DefaultSize, 
                                            wx.DP_DROPDOWN )
        horSizer.Add( self.pickerTanggal, 1, wx.ALL|wx.EXPAND, 5 )
        self.pickerTanggal.SetValue(db_to_wxDateTime(header.tanggal))
        self.pickerTanggal.Disable()
        
        self.staticJenis = wx.StaticText(self.panel, wx.ID_ANY, jenis,
                                         wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticJenis.Wrap(-1)
        horSizer.Add(self.staticJenis, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)

        self.staticFaktur = wx.StaticText(self.panel, wx.ID_ANY, header.noFaktur,
                                         wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticFaktur.Wrap(-1)
        horSizer.Add(self.staticFaktur, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)        
                
        panelSizer.Add(horSizer, 0, wx.ALL|wx.EXPAND, 5)

        horSizer = wx.BoxSizer(wx.HORIZONTAL)
        
        if hasattr(header, 'supplier'):
            partner = getattr(header, 'supplier')
        elif hasattr(header, 'customer'):
            partner = getattr(header, 'customer')
        else:
            partner = None
        if partner is not None:
            self.partner = wx.StaticText(self.panel, wx.ID_ANY, 'Supplier/Customer: %s | %s' % (partner.kode, 
                                                                             partner.nama),
                                         wx.DefaultPosition, wx.DefaultSize, 0)
        else:
            self.partner = wx.StaticText(self.panel, wx.ID_ANY, 'Supplier/Customer: ',
                                         wx.DefaultPosition, wx.DefaultSize, 0)            
        self.partner.Wrap(-1)
        horSizer.Add(self.partner, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
        
        self.createdBy = wx.StaticText(self.panel, wx.ID_ANY, 'created by: %s' % header.createdBy.user,
                                       wx.DefaultPosition, wx.DefaultSize, 0)
        self.createdBy.Wrap(-1)
        horSizer.Add(self.createdBy, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
                    
        panelSizer.Add(horSizer, 0, wx.ALL|wx.EXPAND, 5)
        
        # object list
        self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, 
                                           style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        panelSizer.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
        
        horSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.staticTotal = wx.StaticText(self.panel, wx.ID_ANY, 'Total',
                                         wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTotal.Wrap(-1)
        horSizer.Add(self.staticTotal, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
        
        self.numTotal = NumCtrl(self.panel, wx.ID_ANY, 
                                groupDigits=True,groupChar='.',
                                decimalChar=',',selectOnEntry=True,integerWidth=20)
        horSizer.Add( self.numTotal, 1, wx.ALL|wx.EXPAND, 5 )
        self.numTotal.SetEditable(False)

        panelSizer.Add(horSizer, 0, wx.ALL|wx.ALIGN_RIGHT, 5)
        
        self.panel.SetSizer( panelSizer )
        self.panel.Layout()
        panelSizer.Fit( self.panel )
        sizer.Add( self.panel, 1, wx.EXPAND, 5 )            
        
        self.SetSizer(sizer)
        self.Layout()            

        if isinstance(header, pj.JualHeader):
            colHarga = ColumnDefn("Harga", "right", 100, "hargaJual", stringConverter=integerFormatString)
        else:
            colHarga = ColumnDefn("Harga", "right", 100, "harga", stringConverter=integerFormatString)
        self.objListDetail.SetColumns([                        
            ColumnDefn("Barang", "left", 200, "barang"),
            ColumnDefn("Qty", "right", 80, "qty", stringConverter=integerFormatString),
            colHarga,
            ColumnDefn("Sub Total", "right", 150, self.hitungSubTotal, stringConverter=integerFormatString)
        ])
        if isinstance(header, pj.PenyesuaianStockHeader):
            colKeterangan = ColumnDefn("Keterangan", "left", 200, self.getKeterangan)
            self.objListDetail.AddColumnDefn(colKeterangan)
        
        self.objListDetail.SetObjects(header.details)
        self.hitungTotal()
        
    def getKeterangan(self, item):
        return item.header.keterangan
    
    def hitungTotal(self):
        total = 0
        for row in self.objListDetail.GetObjects():
            subtotal = self.hitungSubTotal(row)
            total = total + subtotal
             
        self.numTotal.SetValue(total)

    def hitungSubTotal(self, item):
        harga = getattr(item, 'harga', getattr(item, 'hargaJual', 0))
        return item.qty * harga