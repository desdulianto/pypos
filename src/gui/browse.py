'''
Created on Jan 2, 2012

@author: david
'''
import wx
from ObjectListView import ObjectListView, ColumnDefn, Filter
from db import pelitajaya as pj
from sqlalchemy import and_, or_

class DialogBrowseData(wx.Dialog):
    def __init__( self, parent ):
        wx.Dialog.__init__  ( self, parent, id = wx.ID_ANY, 
                              title = u'Browse Data', 
                              pos = wx.DefaultPosition, 
                              size = wx.DefaultSize, 
                              style = wx.DEFAULT_DIALOG_STYLE )
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        bSizer2 = wx.BoxSizer( wx.VERTICAL )
        
        bSizer2.SetMinSize( wx.Size( 800, 480 ) ) 
        fgSizer1 = wx.FlexGridSizer( 2, 2, 0, 0 )
        fgSizer1.AddGrowableCol( 1 )
        fgSizer1.SetFlexibleDirection( wx.BOTH )
        fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.staticLabel = wx.StaticText( self, wx.ID_ANY, u"Kunci", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.staticLabel.Wrap( -1 )
        fgSizer1.Add( self.staticLabel, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
        
        self.textCari = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER )
        fgSizer1.Add( self.textCari, 0, wx.ALL|wx.EXPAND, 5 )
                
        bSizer2.Add( fgSizer1, 0, wx.EXPAND, 5 )
       
        self.objListDetail = ObjectListView(self, wx.ID_ANY, 
                                            style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        bSizer2.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
        
        self.SetSizer( bSizer2 )
        self.Layout()
        bSizer2.Fit( self )
        
        self.Centre( wx.BOTH )
        
        self.objListDetail.SetEmptyListMsg("Tidak ada data")
        self.escape = False
        self.selected = None
        
        self.objListDetail.Bind(wx.EVT_LEFT_DCLICK, self.OnSelectItem)
        self.objListDetail.Bind(wx.EVT_KEY_DOWN, self.OnSelectItem)
        
        self.textCari.SetFocus()
        self.textCari.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
    
    def OnKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_ESCAPE,):
            self.escape = True
            self.Close()
        elif keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            if self.objListDetail.GetItemCount() == 1:
                self.selected = self.objListDetail.GetObjectAt(0)
                self.Close()
        elif keyCode == wx.WXK_DOWN:
            if self.objListDetail.GetItemCount() > 0:
                self.objListDetail.SetFocus()
                self.objListDetail.SelectObject(self.objListDetail.GetObjectAt(0))            
        self.filter(event)
        
    def filter(self, event):
        event.Skip()

    def OnSelectItem(self, event):
        if isinstance(event, wx._core.KeyEvent):
            keyCode = event.GetKeyCode()
            if keyCode == wx.WXK_ESCAPE:
                self.objListDetail.DeselectAll()
                self.textCari.SetFocus()
                return
            elif keyCode == wx.WXK_UP:
                if self.objListDetail.GetFocusedRow() == 0:
                    self.objListDetail.DeselectAll()
                    self.textCari.SetFocus()
                else:
                    event.Skip()
                return
            elif keyCode not in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
                event.Skip()
                return
        
        olv = self.objListDetail
        self.selected = olv.GetSelectedObject()
        self.Close()        
    
    def __del__( self ):
        pass
    
class DialogBrowseKategori(DialogBrowseData):
    def __init__(self, parent):
        DialogBrowseData.__init__(self, parent)
        self.objListDetail.SetColumns([
                                       ColumnDefn("Nama", "left", 250, "nama")])
        
        self.db = pj.Kategori
    
    def filter(self, event):
        cari = '%s%s%s' % ('%', self.textCari.GetValue().strip().upper(), '%')
        self.objListDetail.SetObjects([])
        
        objs = self.db.query.filter(self.db.nama.like(cari))
        self.objListDetail.SetObjects(objs)
        event.Skip()
        

class DialogBrowseSatuan(DialogBrowseKategori):
    def __init__(self, parent):
        DialogBrowseKategori.__init__(self, parent)
        self.db = pj.Satuan


class DialogBrowseSupplier(DialogBrowseData):
    def __init__(self, parent):
        DialogBrowseData.__init__(self, parent)
        self.objListDetail.SetColumns([
                                       ColumnDefn("Kode", "left", 100, "kode"),
                                       ColumnDefn("Nama", "left", 250, "nama", isSpaceFilling=True),
                                       ColumnDefn("Telepon", "left", 150, "noTelp"),
                                       ColumnDefn("Alamat", "left", 250, "alamat"),
                                       ColumnDefn("Kota", "left", 150, "kota")])

        self.db = pj.Supplier
                
    def filter(self, event):        
        cari = '%s%s%s' % ('%', self.textCari.GetValue().strip().upper(), '%')
        self.objListDetail.SetObjects([])
        
        objs = self.db.query.filter(or_(self.db.kode.like(cari),
                                        self.db.nama.like(cari),
                                        self.db.alamat.like(cari),
                                        self.db.noTelp.like(cari),
                                        self.db.kota.like(cari)))
        self.objListDetail.SetObjects(objs)
        event.Skip()        
                
class DialogBrowseCustomer(DialogBrowseSupplier):
        def __init__(self, parent):
            DialogBrowseSupplier.__init__(self, parent)
            self.db = pj.Customer
            
class DialogBrowseBarang(DialogBrowseSupplier):
    def __init__(self, parent):
        DialogBrowseSupplier.__init__(self, parent)
        self.db = pj.Barang
        self.objListDetail.DeleteAllColumns()
        self.objListDetail.SetColumns([
                                       ColumnDefn("Kode", "left", 100, "kode"),
                                       ColumnDefn("Nama", "left", 450, "nama", isSpaceFilling=True),
                                       ColumnDefn("Kategori", "left", 250, "kategori"),
                                       ColumnDefn("Satuan", "left", 150, "satuan")])
        
    def filter(self, event):        
        cari = '%s%s%s' % ('%', self.textCari.GetValue().strip().upper(), '%')
        self.objListDetail.SetObjects([])
        
        objs = self.db.query.filter(or_(self.db.kode.like(cari),
                                        self.db.nama.like(cari)))
        self.objListDetail.SetObjects(objs)
        event.Skip()        
