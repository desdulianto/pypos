'''
Created on Dec 20, 2011

@author: david
'''

import wx

class InfoTextCtrl(wx.PyPanel):
    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=wx.TAB_TRAVERSAL,
                 name="InfoTextCtrl"):
        wx.PyPanel.__init__(self, parent, id, pos, size, style, name)
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.text = wx.TextCtrl(self, style=wx.TE_PROCESS_ENTER)
        self.info = wx.StaticText(self, size=self.text.GetSize())
        sizer.Add(self.text)
        sizer.Add(self.info)   
        
        self.SetSizer(sizer)
        self.Layout()
        
        self.GetValue = self.text.GetValue
        self.GetLabel = self.info.GetLabel
        self.SetValue = self.text.SetValue
        self.SetLabel = self.info.SetLabel
        