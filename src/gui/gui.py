# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Oct 27 2009)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx.aui
import wx
from wx.lib.masked import NumCtrl
from ObjectListView import ObjectListView
import wx.calendar
from ObjectListView import GroupListView

###########################################################################
## Class FrameTransaksiPembelian
###########################################################################

class FrameTransaksiPembelian ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Transaksi Pembelian", pos = wx.DefaultPosition, size = wx.Size( 800,600 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticTanggal = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTanggal.Wrap( -1 )
		bSizer3.Add( self.staticTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, 
											wx.DefaultDateTime, 
											wx.DefaultPosition, 
											wx.DefaultSize, 
											wx.DP_DROPDOWN )
		bSizer3.Add( self.pickerTanggal, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.staticNoFaktur = wx.StaticText( self.panel, wx.ID_ANY, u"No Faktur", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoFaktur.Wrap( -1 )
		bSizer3.Add( self.staticNoFaktur, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNoFaktur = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, 
										wx.DefaultPosition, 
										wx.DefaultSize, 0 )
		bSizer3.Add( self.textNoFaktur, 1, wx.ALL|wx.EXPAND, 5 )

		
		self.staticSupplier = wx.StaticText( self.panel, wx.ID_ANY, u"Supplier", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSupplier.Wrap( -1 )
		bSizer3.Add( self.staticSupplier, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textSupplier = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER )
		bSizer3.Add( self.textSupplier,1, wx.ALL, 5 )

		self.staticNamaSupplier = wx.StaticText( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNamaSupplier.Wrap( -1 )
		self.staticNamaSupplier.SetMinSize( wx.Size( 200,-1 ) )

		bSizer3.Add( self.staticNamaSupplier, 3, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer2.Add( bSizer3, 0, wx.EXPAND, 5 )
		
		fgSizer1 = wx.FlexGridSizer( 2, 6, 0, 0 )
		fgSizer1.AddGrowableCol( 0 )
		fgSizer1.AddGrowableCol( 1 )
		fgSizer1.AddGrowableRow( 1 )
		fgSizer1.SetFlexibleDirection( wx.BOTH )
		fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticBarang = wx.StaticText( self.panel, wx.ID_ANY, u"Barang", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticBarang.Wrap( -1 )
		fgSizer1.Add( self.staticBarang, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.staticQty = wx.StaticText( self.panel, wx.ID_ANY, u"Qty", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticQty.Wrap( -1 )
		fgSizer1.Add( self.staticQty, 0, wx.ALL, 5 )

		self.staticHarga = wx.StaticText( self.panel, wx.ID_ANY, u"Harga", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticHarga.Wrap( -1 )
		fgSizer1.Add( self.staticHarga, 0, wx.ALL, 5 )	
		
		self.staticSubTotal = wx.StaticText( self.panel, wx.ID_ANY, u"Sub Total", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSubTotal.Wrap( -1 )
		fgSizer1.Add( self.staticSubTotal, 0, wx.ALL, 5 )
		
		
		
		fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.textKodeBarang = wx.TextCtrl(self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
										wx.Size(200,-1), wx.TE_PROCESS_ENTER)
		fgSizer1.Add(self.textKodeBarang,1, wx.ALL|wx.EXPAND, 5)
		self.staticNamaBarang = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
											wx.DefaultSize, 0)
		fgSizer1.Add(self.staticNamaBarang, 3, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 0)
		
		self.numQty = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		fgSizer1.Add( self.numQty, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.numHarga = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		fgSizer1.Add( self.numHarga, 1, wx.ALL|wx.EXPAND, 5 )
				
		self.numSubTotal = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		self.numSubTotal.SetEditable(False)
		fgSizer1.Add( self.numSubTotal, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.buttonTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer1.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		bSizer2.Add( fgSizer1, 0, wx.EXPAND, 5 )
		
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer4.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer2.Add( bSizer4, 1, wx.EXPAND, 5 )
		
		bSizer92 = wx.BoxSizer( wx.VERTICAL )
		
		fgSizer13 = wx.FlexGridSizer( 4, 2, 0, 0 )
		fgSizer13.AddGrowableCol( 1 )
		fgSizer13.AddGrowableRow( 1 )
		fgSizer13.AddGrowableRow( 2 )
		fgSizer13.AddGrowableRow( 3 )
		fgSizer13.SetFlexibleDirection( wx.BOTH )
		fgSizer13.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticTotal = wx.StaticText( self.panel, wx.ID_ANY, u"Total", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTotal.Wrap( -1 )
		fgSizer13.Add( self.staticTotal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		self.numTotal = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numTotal.SetEditable(False)
		fgSizer13.Add( self.numTotal, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer92.Add( fgSizer13, 0, wx.ALIGN_RIGHT, 5 )
		
		bSizer2.Add( bSizer92, 0, wx.EXPAND, 5 )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		self.buttonSimpan = wx.Button( self.panel, wx.ID_ANY, u"&Simpan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer6.Add( self.buttonSimpan, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		bSizer2.Add( bSizer6, 0, wx.ALIGN_RIGHT, 5 )
		
		self.panel.SetSizer( bSizer2 )
		self.panel.Layout()
		bSizer2.Fit( self.panel )
		bSizer1.Add( self.panel, 1, wx.EXPAND|wx.FIXED_MINSIZE, 5 )
		
		self.SetSizer( bSizer1 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameTransaksiPenjualan
###########################################################################

class FrameTransaksiPenjualan ( wx.aui.AuiMDIChildFrame ):	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, 
											title = u"Transaksi Penjualan", 
											pos = wx.DefaultPosition, 
											size = wx.DefaultSize, 
											style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, 
							   wx.Size( -1,-1 ), wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticTanggal = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal", 
											wx.DefaultPosition, wx.DefaultSize, 
											0 )
		self.staticTanggal.Wrap( -1 )
		bSizer3.Add( self.staticTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, 
												wx.DefaultDateTime, 
												wx.DefaultPosition, 
												wx.DefaultSize, 
												wx.DP_DROPDOWN|wx.NO_BORDER )
		bSizer3.Add( self.pickerTanggal, 2, wx.ALL|wx.EXPAND, 5 )
		
		self.staticNoFaktur = wx.StaticText( self.panel, wx.ID_ANY, 
											 u"No Faktur", wx.DefaultPosition, 
											 wx.DefaultSize, 0 )
		self.staticNoFaktur.Wrap( -1 )
		bSizer3.Add( self.staticNoFaktur, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNoFaktur = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, 
										wx.DefaultPosition, 
										wx.DefaultSize, wx.NO_BORDER )
		self.textNoFaktur.Disable()
		bSizer3.Add( self.textNoFaktur, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.staticCustomer = wx.StaticText( self.panel, wx.ID_ANY, u"Customer", 
											wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticCustomer.Wrap( -1 )
		self.staticCustomer.SetFont( wx.Font( 20, 70, 90, 92, False, wx.EmptyString ) )
		bSizer3.Add( self.staticCustomer, 6, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )		
		
		bSizer2.Add( bSizer3, 0, wx.EXPAND, 5 )
		
		gbSizer = wx.GridBagSizer(5, 5)
		
		self.textKodeBarang = wx.TextCtrl(self.panel, wx.ID_ANY, wx.EmptyString,
										  wx.DefaultPosition, 
										  wx.DefaultSize,
										  wx.TE_PROCESS_ENTER|wx.NO_BORDER)
		self.textKodeBarang.SetFont( wx.Font( 20, 70, 90, 92, False, wx.EmptyString ) )
		gbSizer.Add(self.textKodeBarang, wx.GBPosition(0, 0),
					wx.GBSpan(1, 7), wx.ALL|wx.EXPAND, 5)
		
		self.numQty = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,
								groupDigits=True,groupChar='.',
								decimalChar=',',
								selectOnEntry=True,
								integerWidth=7)
		self.numQty.Disable()
		gbSizer.Add(self.numQty, wx.GBPosition(1, 0), wx.GBSpan(1, 1), 
					wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
		
		self.staticSatuan = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString,
										  wx.DefaultPosition, 
										  wx.Size(70, -1),
										  0)
		gbSizer.Add(self.staticSatuan, wx.GBPosition(1, 1), wx.GBSpan(1, 1),
					wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
		
		self.staticNamaBarang = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString,
											  wx.DefaultPosition, 
											  wx.Size(300, -1),
											  0)
		gbSizer.Add(self.staticNamaBarang, wx.GBPosition(1, 2), wx.GBSpan(1, 1),
					wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
		
		self.staticAt = wx.StaticText(self.panel, wx.ID_ANY, u'@',
									  wx.DefaultPosition, wx.DefaultSize, 0)
		gbSizer.Add(self.staticAt, wx.GBPosition(1, 3), wx.GBSpan(1, 1),
					wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5)
		
		self.numHarga = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,
								groupDigits=True,groupChar='.',
								decimalChar=',',
								selectOnEntry=True,
								integerWidth=15)
		self.numHarga.Disable()
		gbSizer.Add(self.numHarga, wx.GBPosition(1, 4), wx.GBSpan(1, 1),
					wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
		
		self.numSubTotal = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,
								groupDigits=True,groupChar='.',
								decimalChar=',',
								selectOnEntry=True,
								integerWidth=20)
		self.numSubTotal.Disable()
		gbSizer.Add(self.numSubTotal, wx.GBPosition(1, 5), wx.GBSpan(1, 1),
					wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
				
		bSizer2.Add(gbSizer, 0, wx.EXPAND, 5)
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, 
											style=wx.LC_REPORT|wx.SUNKEN_BORDER|wx.WANTS_CHARS)
		bSizer2.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer3 = wx.BoxSizer(wx.HORIZONTAL)
		
		
		self.staticKeterangan = wx.StaticText(self.panel, wx.ID_ANY, 
											u'F2: Qty, F3: Customer, F4: Harga, F5: Browse Barang, Ctrl-S: Simpan/Bayar',
											)
		self.staticKeterangan.Wrap(-1)
		bSizer3.Add(self.staticKeterangan, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT|wx.ALL, 5)		
		
		
		self.staticTotal = wx.StaticText(self.panel, wx.ID_ANY, u'Total',
										 wx.DefaultPosition, wx.DefaultSize, 0)
		self.staticTotal.Wrap(-1)
		font = self.staticTotal.GetFont()
		font.SetWeight(wx.BOLD)
		font.SetPointSize(20)
		self.staticTotal.SetFont(font)
		bSizer3.Add(self.staticTotal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
		
		self.numTotal = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,
								groupDigits=True,groupChar='.',
								decimalChar=',',
								selectOnEntry=True,
								integerWidth=19, style=wx.TE_RIGHT)
		self.numTotal.Disable()
		font = self.numTotal.GetFont()
		font.SetWeight(wx.BOLD)
		font.SetPointSize(20)
		self.numTotal.SetFont(font )
		bSizer3.Add( self.numTotal, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		bSizer2.Add(bSizer3, 0, wx.ALL|wx.ALIGN_RIGHT, 5)
		
		self.panel.SetSizer( bSizer2 )
		self.panel.Layout()
		bSizer2.Fit( self.panel )
		bSizer1.Add( self.panel, 1, wx.EXPAND|wx.FIXED_MINSIZE, 5 )		
		
		self.SetSizer( bSizer1 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameTransaksiReturPembelian
###########################################################################

class FrameTransaksiReturPembelian ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Transaksi Retur Pembelian", pos = wx.DefaultPosition, size = wx.Size( 800,600 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer20 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer21 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer22 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticTanggal = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTanggal.Wrap( -1 )
		bSizer22.Add( self.staticTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer22.Add( self.pickerTanggal, 1, wx.ALL|wx.EXPAND, 5 )

		self.staticNoRetur = wx.StaticText( self.panel, wx.ID_ANY, u"No Retur", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoRetur.Wrap( -1 )
		bSizer22.Add( self.staticNoRetur, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textNoRetur = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, 
									wx.DefaultPosition, wx.DefaultSize, 
									0 )
		bSizer22.Add( self.textNoRetur, 1.5, wx.ALL|wx.EXPAND, 5 )
	
		self.staticSupplier = wx.StaticText( self.panel, wx.ID_ANY, u"Supplier", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSupplier.Wrap( -1 )
		bSizer22.Add( self.staticSupplier, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKodeSupplier = wx.TextCtrl( self.panel, wx.ID_ANY, u'', wx.DefaultPosition,
											wx.DefaultSize, wx.TE_PROCESS_ENTER)
		bSizer22.Add( self.textKodeSupplier, 1.5, wx.ALL|wx.EXPAND, 5)
		
		self.staticNamaSupplier = wx.StaticText( self.panel, wx.ID_ANY, u'', wx.DefaultPosition,
											wx.DefaultSize, 0)
		self.staticNamaSupplier.Wrap(-1)
		bSizer22.Add( self.staticNamaSupplier, 3, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
					
		bSizer21.Add( bSizer22, 0, wx.EXPAND, 5 )
		
		fgSizer1 = wx.FlexGridSizer( 2, 6, 0, 0 )
		fgSizer1.AddGrowableCol( 0 )
		fgSizer1.AddGrowableCol( 1 )
		fgSizer1.AddGrowableRow( 1 )
		fgSizer1.SetFlexibleDirection( wx.BOTH )
		fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticBarang = wx.StaticText( self.panel, wx.ID_ANY, u"Barang", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticBarang.Wrap( -1 )
		fgSizer1.Add( self.staticBarang, 0, wx.ALL, 5 )
		
		fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.staticQty = wx.StaticText( self.panel, wx.ID_ANY, u"Qty", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticQty.Wrap( -1 )
		fgSizer1.Add( self.staticQty, 0, wx.ALL, 5 )

		self.staticHarga = wx.StaticText( self.panel, wx.ID_ANY, u"Harga", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticHarga.Wrap( -1 )
		fgSizer1.Add( self.staticHarga, 0, wx.ALL, 5 )	
		
		self.staticSubTotal = wx.StaticText( self.panel, wx.ID_ANY, u"Sub Total", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSubTotal.Wrap( -1 )
		fgSizer1.Add( self.staticSubTotal, 0, wx.ALL, 5 )
		
		
		fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.textKodeBarang = wx.TextCtrl(self.panel, wx.ID_ANY, wx.EmptyString, 
										wx.DefaultPosition, wx.DefaultSize,
										wx.TE_PROCESS_ENTER)
		fgSizer1.Add(self.textKodeBarang, 1, wx.ALL|wx.EXPAND, 5)
		
		self.staticNamaBarang = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString,
											wx.DefaultPosition, wx.DefaultSize,
											0)
		self.staticNamaBarang.Wrap(-1)
		fgSizer1.Add(self.staticNamaBarang, 2, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)		
				
		self.numQty = NumCtrl(self.panel, wx.ID_ANY, 
							style=wx.TE_PROCESS_ENTER|wx.TE_PROCESS_TAB, 
							allowNegative=False,groupDigits=True,
							groupChar='.',
							decimalChar=',',
							selectOnEntry=True)
		fgSizer1.Add( self.numQty, 0, wx.ALL, 5 )

		self.numHarga = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		fgSizer1.Add( self.numHarga, 0, wx.ALL, 5 )
		
		self.numSubTotal = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		self.numSubTotal.SetEditable(False)
		fgSizer1.Add( self.numSubTotal, 0, wx.ALL, 5 )
		
		self.buttonTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer1.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		bSizer21.Add( fgSizer1, 0, wx.EXPAND, 5 )
		
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer4.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer21.Add( bSizer4, 1, wx.EXPAND, 5 )
		
		bSizer5 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticTotal = wx.StaticText( self.panel, wx.ID_ANY, u"Total", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTotal.Wrap( -1 )
		bSizer5.Add( self.staticTotal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		self.numTotal = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numTotal.SetEditable(False)
		bSizer5.Add( self.numTotal, 0, wx.ALL, 5 )
		
		bSizer21.Add( bSizer5, 0, wx.ALIGN_RIGHT, 5 )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		self.buttonSimpan = wx.Button( self.panel, wx.ID_ANY, u"&Simpan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer6.Add( self.buttonSimpan, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		bSizer21.Add( bSizer6, 0, wx.ALIGN_RIGHT, 5 )
		
		self.panel.SetSizer( bSizer21 )
		self.panel.Layout()
		bSizer21.Fit( self.panel )
		bSizer20.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer20 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameTransaksiReturPenjualan
###########################################################################

class FrameTransaksiReturPenjualan ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Transaksi Retur Penjualan", pos = wx.DefaultPosition, size = wx.Size( 800,600 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer20 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer21 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer22 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticTanggal = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTanggal.Wrap( -1 )
		bSizer22.Add( self.staticTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer22.Add( self.pickerTanggal, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.staticNoRetur = wx.StaticText( self.panel, wx.ID_ANY, u"No Retur", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoRetur.Wrap( -1 )
		bSizer22.Add( self.staticNoRetur, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNoRetur = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, 
									wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer22.Add( self.textNoRetur, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.staticCustomer = wx.StaticText( self.panel, wx.ID_ANY, u"Customer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticCustomer.Wrap( -1 )
		bSizer22.Add( self.staticCustomer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKodeCustomer = wx.TextCtrl(self.panel, wx.ID_ANY, wx.EmptyString,
										wx.DefaultPosition, wx.DefaultSize,
										wx.TE_PROCESS_ENTER)
		bSizer22.Add(self.textKodeCustomer, 1.5, wx.ALL|wx.EXPAND, 5)
		
		self.staticNamaCustomer = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString,
											wx.DefaultPosition, wx.DefaultSize,
											0)
		self.staticNamaCustomer.Wrap(-1)
		bSizer22.Add(self.staticNamaCustomer, 3, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
		
		bSizer21.Add( bSizer22, 0, wx.EXPAND, 5 )
		
		fgSizer1 = wx.FlexGridSizer( 2, 6, 0, 0 )
		fgSizer1.AddGrowableCol( 0 )
		fgSizer1.AddGrowableCol( 1 )
		fgSizer1.AddGrowableRow( 1 )
		fgSizer1.SetFlexibleDirection( wx.BOTH )
		fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticBarang = wx.StaticText( self.panel, wx.ID_ANY, u"Barang", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticBarang.Wrap( -1 )
		fgSizer1.Add( self.staticBarang, 0, wx.ALL, 5 )
		
		fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.staticQty = wx.StaticText( self.panel, wx.ID_ANY, u"Qty", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticQty.Wrap( -1 )
		fgSizer1.Add( self.staticQty, 0, wx.ALL, 5 )

		self.staticHarga = wx.StaticText( self.panel, wx.ID_ANY, u"Harga", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticHarga.Wrap( -1 )
		fgSizer1.Add( self.staticHarga, 0, wx.ALL, 5 )
				
		self.staticSubTotal = wx.StaticText( self.panel, wx.ID_ANY, u"Sub Total", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSubTotal.Wrap( -1 )
		fgSizer1.Add( self.staticSubTotal, 0, wx.ALL, 5 )
		
		
		fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.textKodeBarang = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString,
										wx.DefaultPosition, wx.DefaultSize,
										wx.TE_PROCESS_ENTER)
		fgSizer1.Add(self.textKodeBarang, 1, wx.ALL|wx.EXPAND, 5)
		
		self.staticNamaBarang = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString,
											wx.DefaultPosition, wx.DefaultSize,
											0)
		self.staticNamaBarang.Wrap(-1)
		fgSizer1.Add(self.staticNamaBarang, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
		
		self.numQty = NumCtrl(self.panel, wx.ID_ANY, style=wx.TE_PROCESS_ENTER|wx.TE_PROCESS_TAB,
							allowNegative=False,
							groupDigits=True,groupChar='.',
							decimalChar=',',
							selectOnEntry=True)
		fgSizer1.Add( self.numQty, 0, wx.ALL, 5 )

		self.numHarga = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		fgSizer1.Add( self.numHarga, 0, wx.ALL, 5 )
				
		self.numSubTotal = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		self.numSubTotal.SetEditable(False)
		fgSizer1.Add( self.numSubTotal, 0, wx.ALL, 5 )
		
		self.buttonTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer1.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		bSizer21.Add( fgSizer1, 0, wx.EXPAND, 5 )
		
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer4.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer21.Add( bSizer4, 1, wx.EXPAND, 5 )
		
		bSizer5 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticTotal = wx.StaticText( self.panel, wx.ID_ANY, u"Total", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTotal.Wrap( -1 )
		bSizer5.Add( self.staticTotal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		self.numTotal = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numTotal.SetEditable(False)
		bSizer5.Add( self.numTotal, 0, wx.ALL, 5 )
		
		bSizer21.Add( bSizer5, 0, wx.ALIGN_RIGHT, 5 )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		self.buttonSimpan = wx.Button( self.panel, wx.ID_ANY, u"&Simpan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer6.Add( self.buttonSimpan, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		bSizer21.Add( bSizer6, 0, wx.ALIGN_RIGHT, 5 )
		
		self.panel.SetSizer( bSizer21 )
		self.panel.Layout()
		bSizer21.Fit( self.panel )
		bSizer20.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer20 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameDataSupplier
###########################################################################

class FrameDataSupplier ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Data Supplier", pos = wx.DefaultPosition, size = wx.Size( 725,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer25 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer26 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer27 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticKode = wx.StaticText( self.panel, wx.ID_ANY, u"Kode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticKode.Wrap( -1 )
		bSizer27.Add( self.staticKode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKode = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textKode, 1, wx.ALL, 5 )
		
		self.staticNama = wx.StaticText( self.panel, wx.ID_ANY, u"Nama", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNama.Wrap( -1 )
		bSizer27.Add( self.staticNama, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNama = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textNama, 2, wx.ALL, 5 )
		
		self.staticAlamat = wx.StaticText( self.panel, wx.ID_ANY, u"Alamat", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticAlamat.Wrap( -1 )
		bSizer27.Add( self.staticAlamat, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textAlamat = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textAlamat, 1, wx.ALL, 5 )
		
		self.staticNoTelp = wx.StaticText( self.panel, wx.ID_ANY, u"No. Telp", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoTelp.Wrap( -1 )
		bSizer27.Add( self.staticNoTelp, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNoTelp = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textNoTelp, 1, wx.ALL, 5 )
		
		self.staticKota = wx.StaticText( self.panel, wx.ID_ANY, u"Kota", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticKota.Wrap( -1 )
		bSizer27.Add( self.staticKota, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKota = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textKota, 1, wx.ALL, 5 )
				
		self.buttonTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		bSizer26.Add( bSizer27, 0, wx.EXPAND, 5 )
		
		bSizer32 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSaring = wx.StaticText( self.panel, wx.ID_ANY, u"Saring", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSaring.Wrap( -1 )
		bSizer32.Add( self.staticSaring, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textSaring = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer32.Add( self.textSaring, 1, wx.ALL, 5 )
		
		bSizer26.Add( bSizer32, 0, wx.EXPAND, 5 )
		
		bSizer28 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer28.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer26.Add( bSizer28, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer26 )
		self.panel.Layout()
		bSizer26.Fit( self.panel )
		bSizer25.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer25 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameDataCustomer
###########################################################################

class FrameDataCustomer ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Data Customer", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer25 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer26 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer27 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticKode = wx.StaticText( self.panel, wx.ID_ANY, u"Kode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticKode.Wrap( -1 )
		bSizer27.Add( self.staticKode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKode = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textKode, 1, wx.ALL, 5 )
		
		self.staticNama = wx.StaticText( self.panel, wx.ID_ANY, u"Nama", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNama.Wrap( -1 )
		bSizer27.Add( self.staticNama, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNama = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textNama, 2, wx.ALL, 5 )
		
		self.staticAlamat = wx.StaticText( self.panel, wx.ID_ANY, u"Alamat", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticAlamat.Wrap( -1 )
		bSizer27.Add( self.staticAlamat, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textAlamat = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textAlamat, 1, wx.ALL, 5 )
		
		self.staticNoTelp = wx.StaticText( self.panel, wx.ID_ANY, u"No, Telp", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoTelp.Wrap( -1 )
		bSizer27.Add( self.staticNoTelp, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNoTelp = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textNoTelp, 1, wx.ALL, 5 )
		
		self.staticKota = wx.StaticText( self.panel, wx.ID_ANY, u"Kota", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticKota.Wrap( -1 )
		bSizer27.Add( self.staticKota, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKota = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textKota, 1, wx.ALL, 5 )
		
		self.buttonTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		bSizer26.Add( bSizer27, 0, wx.EXPAND, 5 )
		
		bSizer32 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSaring = wx.StaticText( self.panel, wx.ID_ANY, u"Saring", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSaring.Wrap( -1 )
		bSizer32.Add( self.staticSaring, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textSaring = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer32.Add( self.textSaring, 1, wx.ALL, 5 )
		
		bSizer26.Add( bSizer32, 0, wx.EXPAND, 5 )
		
		bSizer28 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer28.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer26.Add( bSizer28, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer26 )
		self.panel.Layout()
		bSizer26.Fit( self.panel )
		bSizer25.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer25 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameDataSales
###########################################################################

class FrameDataSales ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Data Sales", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer25 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer26 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer27 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticKode = wx.StaticText( self.panel, wx.ID_ANY, u"Kode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticKode.Wrap( -1 )
		bSizer27.Add( self.staticKode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKode = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textKode, 1, wx.ALL, 5 )
		
		self.staticNama = wx.StaticText( self.panel, wx.ID_ANY, u"Nama", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNama.Wrap( -1 )
		bSizer27.Add( self.staticNama, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNama = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textNama, 2, wx.ALL, 5 )
		
		self.buttonTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		bSizer26.Add( bSizer27, 0, wx.EXPAND, 5 )
		
		bSizer32 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSaring = wx.StaticText( self.panel, wx.ID_ANY, u"Saring", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSaring.Wrap( -1 )
		bSizer32.Add( self.staticSaring, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textSaring = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer32.Add( self.textSaring, 1, wx.ALL, 5 )
		
		bSizer26.Add( bSizer32, 0, wx.EXPAND, 5 )
		
		bSizer28 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer28.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer26.Add( bSizer28, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer26 )
		self.panel.Layout()
		bSizer26.Fit( self.panel )
		bSizer25.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer25 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameDataBarang
###########################################################################

class FrameDataBarang ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Data Barang", pos = wx.DefaultPosition, size = wx.Size( 800,600 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer44 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer45 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer46 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticKode = wx.StaticText( self.panel, wx.ID_ANY, u"Kode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticKode.Wrap( -1 )
		bSizer46.Add( self.staticKode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKode = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer46.Add( self.textKode, 2, wx.ALL, 5 )
		
		self.staticNama = wx.StaticText( self.panel, wx.ID_ANY, u"Nama", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNama.Wrap( -1 )
		bSizer46.Add( self.staticNama, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNama = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer46.Add( self.textNama, 3, wx.ALL, 5 )
				
		self.staticHargaJual = wx.StaticText( self.panel, wx.ID_ANY, u"Harga Jual", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticHargaJual.Wrap( -1 )
		bSizer46.Add( self.staticHargaJual, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.numHarga = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		bSizer46.Add( self.numHarga, 0, wx.ALL, 5 )
		
		self.staticSatuan = wx.StaticText( self.panel, wx.ID_ANY, u"Satuan", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSatuan.Wrap( -1 )
		bSizer46.Add( self.staticSatuan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textSatuan = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer46.Add( self.textSatuan, 1, wx.ALL, 5 )
		
		self.staticKategori = wx.StaticText( self.panel, wx.ID_ANY, u"Kategori", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticKategori.Wrap( -1 )
		bSizer46.Add( self.staticKategori, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKategori = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer46.Add( self.textKategori, 1, wx.ALL, 5 )


		self.buttonTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer46.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		bSizer45.Add( bSizer46, 0, wx.EXPAND, 5 )
		
		bSizer32 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSaring = wx.StaticText( self.panel, wx.ID_ANY, u"Saring", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSaring.Wrap( -1 )
		bSizer32.Add( self.staticSaring, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textSaring = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer32.Add( self.textSaring, 1, wx.ALL, 5 )
		
		bSizer45.Add( bSizer32, 0, wx.EXPAND, 5 )
		
		bSizer48 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer48.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer45.Add( bSizer48, 1, wx.EXPAND, 5 )
		
		bSizer68 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonPrint = wx.Button( self.panel, wx.ID_ANY, u"&Print", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer68.Add( self.buttonPrint, 0, wx.ALL, 5 )
		
		bSizer45.Add( bSizer68, 0, wx.ALIGN_RIGHT, 5 )
		
		self.panel.SetSizer( bSizer45 )
		self.panel.Layout()
		bSizer45.Fit( self.panel )
		bSizer44.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer44 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameDataHargaJual
###########################################################################

class FrameDataHargaJual ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Harga Jual", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer49 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer50 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer581 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer58 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.static = wx.StaticText( self.panel, wx.ID_ANY, u"Customer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.static.Wrap( -1 )
		bSizer58.Add( self.static, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKodeCustomer = wx.TextCtrl(self.panel, wx.ID_ANY, wx.EmptyString,
										wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
		bSizer58.Add(self.textKodeCustomer, 1, wx.ALL, 5)
		
		self.staticNamaCustomer = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString,
											wx.DefaultPosition, wx.DefaultSize, 0)
		bSizer58.Add(self.staticNamaCustomer, 2, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
		
		
		bSizer581.Add( bSizer58, 0, wx.EXPAND, 5 )
		
		bSizer32 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSaring = wx.StaticText( self.panel, wx.ID_ANY, u"Saring", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSaring.Wrap( -1 )
		bSizer32.Add( self.staticSaring, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textSaring = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer32.Add( self.textSaring, 1, wx.ALL, 5 )
		
		bSizer581.Add( bSizer32, 1, wx.EXPAND, 5 )
		
		bSizer50.Add( bSizer581, 0, wx.EXPAND, 5 )
		
		bSizer60 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer60.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer50.Add( bSizer60, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer50 )
		self.panel.Layout()
		bSizer50.Fit( self.panel )
		bSizer49.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer49 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameDataSatuan
###########################################################################

class FrameDataSatuan ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Data Satuan", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer25 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer26 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer27 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticNama = wx.StaticText( self.panel, wx.ID_ANY, u"Nama", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNama.Wrap( -1 )
		bSizer27.Add( self.staticNama, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNama = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textNama, 2, wx.ALL, 5 )
		
		self.buttonTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		bSizer26.Add( bSizer27, 0, wx.EXPAND, 5 )
		
		bSizer32 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSaring = wx.StaticText( self.panel, wx.ID_ANY, u"Saring", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSaring.Wrap( -1 )
		bSizer32.Add( self.staticSaring, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textSaring = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer32.Add( self.textSaring, 1, wx.ALL, 5 )
		
		bSizer26.Add( bSizer32, 0, wx.EXPAND, 5 )
		
		bSizer28 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer28.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer26.Add( bSizer28, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer26 )
		self.panel.Layout()
		bSizer26.Fit( self.panel )
		bSizer25.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer25 )
		self.Layout()
		
		self.textNama.SetFocus()
	
	def __del__( self ):
		pass


###########################################################################
## Class FrameDataKategori
###########################################################################

class FrameDataKategori ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Data Kategori", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer25 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer26 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer27 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticNama = wx.StaticText( self.panel, wx.ID_ANY, u"Nama", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNama.Wrap( -1 )
		bSizer27.Add( self.staticNama, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNama = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textNama, 2, wx.ALL, 5 )
		
		self.buttonTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		bSizer26.Add( bSizer27, 0, wx.EXPAND, 5 )
		
		bSizer32 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSaring = wx.StaticText( self.panel, wx.ID_ANY, u"Saring", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSaring.Wrap( -1 )
		bSizer32.Add( self.staticSaring, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textSaring = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer32.Add( self.textSaring, 1, wx.ALL, 5 )
		
		bSizer26.Add( bSizer32, 0, wx.EXPAND, 5 )
		
		bSizer28 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer28.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer26.Add( bSizer28, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer26 )
		self.panel.Layout()
		bSizer26.Fit( self.panel )
		bSizer25.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer25 )
		self.Layout()
		
		self.textNama.SetFocus()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class DialogDataSupplier
###########################################################################

class DialogDataSupplier ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__  ( self, parent, id = wx.ID_ANY, title = u"Data Supplier", pos = wx.DefaultPosition, size = wx.Size( 640,480 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer26 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer27 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticKode = wx.StaticText( self, wx.ID_ANY, u"Kode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticKode.Wrap( -1 )
		bSizer27.Add( self.staticKode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textKode = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textKode, 1, wx.ALL, 5 )
		
		self.staticNama = wx.StaticText( self, wx.ID_ANY, u"Nama", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNama.Wrap( -1 )
		bSizer27.Add( self.staticNama, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNama = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.textNama, 2, wx.ALL, 5 )
		
		self.buttonTambah = wx.Button( self, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		self.buttonSaring = wx.Button( self, wx.ID_ANY, u"&Saring", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.buttonSaring, 0, wx.ALL, 5 )
		
		bSizer26.Add( bSizer27, 0, wx.EXPAND, 5 )
		
		bSizer28 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer28.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer26.Add( bSizer28, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer26 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class DialogLogin
###########################################################################

class DialogLogin ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__  ( self, parent, id = wx.ID_ANY, title = u"Login", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE|wx.DIALOG_NO_PARENT )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		fgSizer6 = wx.FlexGridSizer( 3, 2, 5, 5 )
		fgSizer6.AddGrowableCol( 1 )
		fgSizer6.SetFlexibleDirection( wx.BOTH )
		fgSizer6.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticUser = wx.StaticText( self, wx.ID_ANY, u"User", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticUser.Wrap( -1 )
		fgSizer6.Add( self.staticUser, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		self.textUser = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer6.Add( self.textUser, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.staticPassword = wx.StaticText( self, wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPassword.Wrap( -1 )
		fgSizer6.Add( self.staticPassword, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		self.textPassword = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		fgSizer6.Add( self.textPassword, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer6.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		bSizer57 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonLogin = wx.Button( self, wx.ID_OK, u"&Login", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer57.Add( self.buttonLogin, 0, wx.ALL, 5 )
		
		self.buttonBatal = wx.Button( self, wx.ID_CANCEL, u"&Batal", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer57.Add( self.buttonBatal, 0, wx.ALL, 5 )
		
		fgSizer6.Add( bSizer57, 1, wx.EXPAND, 5 )
		
		self.SetSizer( fgSizer6 )
		self.Layout()
		fgSizer6.Fit( self )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FramePembayaranHutang
###########################################################################

class FramePembayaranHutang ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Supplier | Pembayaran", pos = wx.DefaultPosition, size = wx.Size( 803,378 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer60 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer62 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer63 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSupplier = wx.StaticText( self.panel, wx.ID_ANY, u"Supplier", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSupplier.Wrap( -1 )
		bSizer63.Add( self.staticSupplier, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		comboSupplierChoices = []
		self.comboSupplier = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboSupplierChoices, wx.CB_DROPDOWN )
		bSizer63.Add( self.comboSupplier, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticTanggal = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTanggal.Wrap( -1 )
		bSizer63.Add( self.staticTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer63.Add( self.pickerTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticNoFaktur = wx.StaticText( self.panel, wx.ID_ANY, u"No. Faktur", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoFaktur.Wrap( -1 )
		bSizer63.Add( self.staticNoFaktur, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNoFaktur = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer63.Add( self.textNoFaktur, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		bSizer62.Add( bSizer63, 0, wx.EXPAND, 5 )
		
		bSizer80 = wx.BoxSizer( wx.HORIZONTAL )
		
		radioBoxCaraBayarChoices = [ u"Cash", u"Giro" ]
		self.radioBoxCaraBayar = wx.RadioBox( self.panel, wx.ID_ANY, u"Cara Bayar", wx.DefaultPosition, wx.DefaultSize, radioBoxCaraBayarChoices, 2, wx.RA_SPECIFY_COLS )
		self.radioBoxCaraBayar.SetSelection( 0 )
		bSizer80.Add( self.radioBoxCaraBayar, 0, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer10 = wx.FlexGridSizer( 2, 3, 0, 0 )
		fgSizer10.AddGrowableCol( 0 )
		fgSizer10.AddGrowableCol( 1 )
		fgSizer10.AddGrowableRow( 1 )
		fgSizer10.SetFlexibleDirection( wx.BOTH )
		fgSizer10.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticBank = wx.StaticText( self.panel, wx.ID_ANY, u"Bank", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticBank.Wrap( -1 )
		fgSizer10.Add( self.staticBank, 0, wx.ALL, 5 )
		
		self.staticNoGiro = wx.StaticText( self.panel, wx.ID_ANY, u"No. Giro", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoGiro.Wrap( -1 )
		fgSizer10.Add( self.staticNoGiro, 0, wx.ALL, 5 )
		
		self.staticJatuhTempo = wx.StaticText( self.panel, wx.ID_ANY, u"Jatuh Tempo", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticJatuhTempo.Wrap( -1 )
		fgSizer10.Add( self.staticJatuhTempo, 0, wx.ALL, 5 )
		
		comboBankChoices = []
		self.comboBank = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboBankChoices, 0 )
		fgSizer10.Add( self.comboBank, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.textNoGiro = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer10.Add( self.textNoGiro, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.pickerJatuhTempo = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		fgSizer10.Add( self.pickerJatuhTempo, 0, wx.ALL, 5 )
		
		bSizer80.Add( fgSizer10, 1, wx.EXPAND, 5 )
		
		radioBoxJenisBayarChoices = [ u"Per Faktur", u"Lump Sum" ]
		self.radioBoxJenisBayar = wx.RadioBox( self.panel, wx.ID_ANY, u"Jenis Pembayaran", wx.DefaultPosition, wx.DefaultSize, radioBoxJenisBayarChoices, 2, wx.RA_SPECIFY_COLS )
		self.radioBoxJenisBayar.SetSelection( 0 )
		bSizer80.Add( self.radioBoxJenisBayar, 0, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer11 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer11.AddGrowableCol( 0 )
		fgSizer11.AddGrowableRow( 1 )
		fgSizer11.SetFlexibleDirection( wx.BOTH )
		fgSizer11.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticBayar = wx.StaticText( self.panel, wx.ID_ANY, u"Jumlah Bayar", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticBayar.Wrap( -1 )
		fgSizer11.Add( self.staticBayar, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.numBayar = NumCtrl(self.panel, wx.ID_ANY, integerWidth=20, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		fgSizer11.Add( self.numBayar, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 5 )
		
		bSizer80.Add( fgSizer11, 0, wx.EXPAND, 5 )
		
		self.buttonBayar = wx.Button( self.panel, wx.ID_ANY, u"&Bayar", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer80.Add( self.buttonBayar, 0, wx.ALL|wx.EXPAND, 5 )
		
		bSizer62.Add( bSizer80, 0, wx.EXPAND, 5 )
		
		bSizer64 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer64.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer62.Add( bSizer64, 1, wx.EXPAND, 5 )
		
		fgSizer6 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fgSizer6.SetFlexibleDirection( wx.BOTH )
		fgSizer6.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticDeposit = wx.StaticText( self.panel, wx.ID_ANY, u"Deposit", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticDeposit.Wrap( -1 )
		fgSizer6.Add( self.staticDeposit, 0, wx.ALL, 5 )
		
		self.numDeposit = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numDeposit.SetEditable(False)
		fgSizer6.Add( self.numDeposit, 0, wx.ALL, 5 )
		
		self.staticTotalSisaKredit = wx.StaticText( self.panel, wx.ID_ANY, u"Total Sisa Kredit", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTotalSisaKredit.Wrap( -1 )
		fgSizer6.Add( self.staticTotalSisaKredit, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.numTotalSisaKredit = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numTotalSisaKredit.SetEditable(False)
		fgSizer6.Add( self.numTotalSisaKredit, 0, wx.ALL, 5 )
		
		self.staticTotalBayar = wx.StaticText( self.panel, wx.ID_ANY, u"Total Pembayaran", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTotalBayar.Wrap( -1 )
		fgSizer6.Add( self.staticTotalBayar, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.numTotalBayar = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numTotalBayar.SetEditable(False)
		fgSizer6.Add( self.numTotalBayar, 1, wx.ALL, 5 )
		
		self.staticSisaPembayaran = wx.StaticText( self.panel, wx.ID_ANY, u"Sisa Pembayaran", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSisaPembayaran.Wrap( -1 )
		fgSizer6.Add( self.staticSisaPembayaran, 0, wx.ALL, 5 )
		
		self.numSisaPembayaran = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numSisaPembayaran.SetEditable(False)
		fgSizer6.Add( self.numSisaPembayaran, 0, wx.ALL, 5 )
		
		self.staticLebihBayar = wx.StaticText( self.panel, wx.ID_ANY, u"Kelebihan Bayar", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticLebihBayar.Wrap( -1 )
		fgSizer6.Add( self.staticLebihBayar, 0, wx.ALL, 5 )
		
		self.numLebihBayar = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numLebihBayar.SetEditable(False)
		fgSizer6.Add( self.numLebihBayar, 0, wx.ALL, 5 )
		
		self.staticSisaDeposit = wx.StaticText( self.panel, wx.ID_ANY, u"Sisa Deposit", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSisaDeposit.Wrap( -1 )
		fgSizer6.Add( self.staticSisaDeposit, 0, wx.ALL, 5 )
		
		self.numSisaDeposit = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numSisaDeposit.SetEditable(False)
		fgSizer6.Add( self.numSisaDeposit, 0, wx.ALL, 5 )
		
		bSizer62.Add( fgSizer6, 0, wx.ALIGN_RIGHT, 5 )
		
		bSizer66 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonSimpan = wx.Button( self.panel, wx.ID_ANY, u"&Simpan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer66.Add( self.buttonSimpan, 0, wx.ALL|wx.EXPAND, 5 )
		
		bSizer62.Add( bSizer66, 0, wx.ALIGN_RIGHT, 5 )
		
		self.panel.SetSizer( bSizer62 )
		self.panel.Layout()
		bSizer62.Fit( self.panel )
		bSizer60.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer60 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FramePembayaranPiutang
###########################################################################

class FramePembayaranPiutang ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Customer | Pembayaran", pos = wx.DefaultPosition, size = wx.Size( 803,378 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer60 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer62 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer63 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticCustomer = wx.StaticText( self.panel, wx.ID_ANY, u"Customer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticCustomer.Wrap( -1 )
		bSizer63.Add( self.staticCustomer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		comboCustomerChoices = []
		self.comboCustomer = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboCustomerChoices, wx.CB_DROPDOWN )
		bSizer63.Add( self.comboCustomer, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticTanggal = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTanggal.Wrap( -1 )
		bSizer63.Add( self.staticTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer63.Add( self.pickerTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticNoFaktur = wx.StaticText( self.panel, wx.ID_ANY, u"No. Faktur", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoFaktur.Wrap( -1 )
		bSizer63.Add( self.staticNoFaktur, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNoFaktur = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer63.Add( self.textNoFaktur, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		bSizer62.Add( bSizer63, 0, wx.EXPAND, 5 )
		
		bSizer80 = wx.BoxSizer( wx.HORIZONTAL )
		
		radioBoxCaraBayarChoices = [ u"Cash", u"Giro" ]
		self.radioBoxCaraBayar = wx.RadioBox( self.panel, wx.ID_ANY, u"Cara Bayar", wx.DefaultPosition, wx.DefaultSize, radioBoxCaraBayarChoices, 2, wx.RA_SPECIFY_COLS )
		self.radioBoxCaraBayar.SetSelection( 0 )
		bSizer80.Add( self.radioBoxCaraBayar, 0, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer10 = wx.FlexGridSizer( 2, 3, 0, 0 )
		fgSizer10.AddGrowableCol( 0 )
		fgSizer10.AddGrowableCol( 1 )
		fgSizer10.AddGrowableRow( 1 )
		fgSizer10.SetFlexibleDirection( wx.BOTH )
		fgSizer10.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticBank = wx.StaticText( self.panel, wx.ID_ANY, u"Bank", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticBank.Wrap( -1 )
		fgSizer10.Add( self.staticBank, 0, wx.ALL, 5 )
		
		self.staticNoGiro = wx.StaticText( self.panel, wx.ID_ANY, u"No. Giro", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoGiro.Wrap( -1 )
		fgSizer10.Add( self.staticNoGiro, 0, wx.ALL, 5 )
		
		self.staticJatuhTempo = wx.StaticText( self.panel, wx.ID_ANY, u"Jatuh Tempo", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticJatuhTempo.Wrap( -1 )
		fgSizer10.Add( self.staticJatuhTempo, 0, wx.ALL, 5 )
		
		comboBankChoices = []
		self.comboBank = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboBankChoices, 0 )
		fgSizer10.Add( self.comboBank, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.textNoGiro = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer10.Add( self.textNoGiro, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.pickerJatuhTempo = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		fgSizer10.Add( self.pickerJatuhTempo, 0, wx.ALL, 5 )
		
		bSizer80.Add( fgSizer10, 1, wx.EXPAND, 5 )
		
		radioBoxJenisBayarChoices = [ u"Per Faktur", u"Lump Sum" ]
		self.radioBoxJenisBayar = wx.RadioBox( self.panel, wx.ID_ANY, u"Jenis Pembayaran", wx.DefaultPosition, wx.DefaultSize, radioBoxJenisBayarChoices, 2, wx.RA_SPECIFY_COLS )
		self.radioBoxJenisBayar.SetSelection( 0 )
		bSizer80.Add( self.radioBoxJenisBayar, 0, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer11 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer11.AddGrowableCol( 0 )
		fgSizer11.AddGrowableRow( 1 )
		fgSizer11.SetFlexibleDirection( wx.BOTH )
		fgSizer11.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticBayar = wx.StaticText( self.panel, wx.ID_ANY, u"Jumlah Bayar", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticBayar.Wrap( -1 )
		fgSizer11.Add( self.staticBayar, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.numBayar = NumCtrl(self.panel, wx.ID_ANY, integerWidth=20, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		fgSizer11.Add( self.numBayar, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 5 )
		
		bSizer80.Add( fgSizer11, 0, wx.EXPAND, 5 )
		
		self.buttonBayar = wx.Button( self.panel, wx.ID_ANY, u"&Bayar", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer80.Add( self.buttonBayar, 0, wx.ALL|wx.EXPAND, 5 )
		
		bSizer62.Add( bSizer80, 0, wx.EXPAND, 5 )
		
		bSizer64 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer64.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer62.Add( bSizer64, 1, wx.EXPAND, 5 )
		
		fgSizer6 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fgSizer6.SetFlexibleDirection( wx.BOTH )
		fgSizer6.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticDeposit = wx.StaticText( self.panel, wx.ID_ANY, u"Deposit", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticDeposit.Wrap( -1 )
		fgSizer6.Add( self.staticDeposit, 0, wx.ALL, 5 )
		
		self.numDeposit = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numDeposit.SetEditable(False)
		fgSizer6.Add( self.numDeposit, 0, wx.ALL, 5 )
		
		self.staticTotalSisaKredit = wx.StaticText( self.panel, wx.ID_ANY, u"Total Sisa Kredit", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTotalSisaKredit.Wrap( -1 )
		fgSizer6.Add( self.staticTotalSisaKredit, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.numTotalSisaKredit = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numTotalSisaKredit.SetEditable(False)
		fgSizer6.Add( self.numTotalSisaKredit, 0, wx.ALL, 5 )
		
		self.staticTotalBayar = wx.StaticText( self.panel, wx.ID_ANY, u"Total Pembayaran", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTotalBayar.Wrap( -1 )
		fgSizer6.Add( self.staticTotalBayar, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.numTotalBayar = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numTotalBayar.SetEditable(False)
		fgSizer6.Add( self.numTotalBayar, 1, wx.ALL, 5 )
		
		self.staticSisaPembayaran = wx.StaticText( self.panel, wx.ID_ANY, u"Sisa Pembayaran", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSisaPembayaran.Wrap( -1 )
		fgSizer6.Add( self.staticSisaPembayaran, 0, wx.ALL, 5 )
		
		self.numSisaPembayaran = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numSisaPembayaran.SetEditable(False)
		fgSizer6.Add( self.numSisaPembayaran, 0, wx.ALL, 5 )
		
		self.staticLebihBayar = wx.StaticText( self.panel, wx.ID_ANY, u"Kelebihan Bayar", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticLebihBayar.Wrap( -1 )
		fgSizer6.Add( self.staticLebihBayar, 0, wx.ALL, 5 )
		
		self.numLebihBayar = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numLebihBayar.SetEditable(False)
		fgSizer6.Add( self.numLebihBayar, 0, wx.ALL, 5 )
		
		self.staticSisaDeposit = wx.StaticText( self.panel, wx.ID_ANY, u"Sisa Deposit", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSisaDeposit.Wrap( -1 )
		fgSizer6.Add( self.staticSisaDeposit, 0, wx.ALL, 5 )
		
		self.numSisaDeposit = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True,integerWidth=20)
		self.numSisaDeposit.SetEditable(False)
		fgSizer6.Add( self.numSisaDeposit, 0, wx.ALL, 5 )
		
		bSizer62.Add( fgSizer6, 0, wx.ALIGN_RIGHT, 5 )
		
		bSizer66 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonSimpan = wx.Button( self.panel, wx.ID_ANY, u"&Simpan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer66.Add( self.buttonSimpan, 0, wx.ALL|wx.EXPAND, 5 )
		
		bSizer62.Add( bSizer66, 0, wx.ALIGN_RIGHT, 5 )
		
		self.panel.SetSizer( bSizer62 )
		self.panel.Layout()
		bSizer62.Fit( self.panel )
		bSizer60.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer60 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class DialogUbahPassword
###########################################################################

class DialogUbahPassword ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__  ( self, parent, id = wx.ID_ANY, title = u"Ubah Password", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer108 = wx.BoxSizer( wx.VERTICAL )
		
		gSizer1 = wx.GridSizer( 4, 2, 0, 0 )
		
		self.staticUser = wx.StaticText( self, wx.ID_ANY, u"User", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticUser.Wrap( -1 )
		gSizer1.Add( self.staticUser, 0, wx.ALL, 5 )
		
		self.staticNamaUser = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNamaUser.Wrap( -1 )
		gSizer1.Add( self.staticNamaUser, 0, wx.ALL, 5 )
		
		self.staticPasswordLama = wx.StaticText( self, wx.ID_ANY, u"Password Lama", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPasswordLama.Wrap( -1 )
		gSizer1.Add( self.staticPasswordLama, 0, wx.ALL, 5 )
		
		self.textPasswordLama = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		gSizer1.Add( self.textPasswordLama, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.staticPasswordBaru = wx.StaticText( self, wx.ID_ANY, u"Password Baru", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPasswordBaru.Wrap( -1 )
		gSizer1.Add( self.staticPasswordBaru, 0, wx.ALL, 5 )
		
		self.textPasswordBaru = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		gSizer1.Add( self.textPasswordBaru, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.staticPasswordBaru1 = wx.StaticText( self, wx.ID_ANY, u"Konfirmasi Password Baru", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPasswordBaru1.Wrap( -1 )
		gSizer1.Add( self.staticPasswordBaru1, 0, wx.ALL, 5 )
		
		self.textPasswordBaru1 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		gSizer1.Add( self.textPasswordBaru1, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer108.Add( gSizer1, 0, wx.EXPAND, 5 )
		
		bSizer113 = wx.BoxSizer( wx.VERTICAL )
		
		self.buttonUbahPassword = wx.Button( self, wx.ID_ANY, u"&Ubah Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer113.Add( self.buttonUbahPassword, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer108.Add( bSizer113, 0, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer108 )
		self.Layout()
		bSizer108.Fit( self )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class DialogUserBaru
###########################################################################

class DialogUserBaru ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__  ( self, parent, id = wx.ID_ANY, title = u"User Baru", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer114 = wx.BoxSizer( wx.VERTICAL )
		
		gSizer2 = wx.GridSizer( 3, 2, 0, 0 )
		
		self.staticNamaUser = wx.StaticText( self, wx.ID_ANY, u"Nama User", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNamaUser.Wrap( -1 )
		gSizer2.Add( self.staticNamaUser, 0, wx.ALL, 5 )
		
		self.textNamaUser = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.textNamaUser, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.staticPassword = wx.StaticText( self, wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPassword.Wrap( -1 )
		gSizer2.Add( self.staticPassword, 0, wx.ALL, 5 )
		
		self.textPassword = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		gSizer2.Add( self.textPassword, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.staticPassword1 = wx.StaticText( self, wx.ID_ANY, u"Konfirmasi Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPassword1.Wrap( -1 )
		gSizer2.Add( self.staticPassword1, 0, wx.ALL, 5 )
		
		self.textPassword1 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		gSizer2.Add( self.textPassword1, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer114.Add( gSizer2, 0, wx.EXPAND, 5 )
		
		bSizer115 = wx.BoxSizer( wx.VERTICAL )
		
		self.buttonTambahUser = wx.Button( self, wx.ID_ANY, u"&Tambah User", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer115.Add( self.buttonTambahUser, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer114.Add( bSizer115, 0, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer114 )
		self.Layout()
		bSizer114.Fit( self )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class DialogKalendar
###########################################################################

class DialogKalendar ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__  ( self, parent, id = wx.ID_ANY, title = u"Kalendar", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer97 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer99 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticTanggalTransaksi = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal Transaksi", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTanggalTransaksi.Wrap( -1 )
		bSizer99.Add( self.staticTanggalTransaksi, 0, wx.ALL, 5 )
		
		self.calendarTanggal = wx.calendar.CalendarCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.calendar.CAL_SHOW_HOLIDAYS )
		bSizer99.Add( self.calendarTanggal, 0, wx.ALL, 5 )
		
		self.panel.SetSizer( bSizer99 )
		self.panel.Layout()
		bSizer99.Fit( self.panel )
		bSizer97.Add( self.panel, 1, wx.EXPAND |wx.ALL, 5 )
		
		bSizer100 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonOK = wx.Button( self, wx.ID_OK, u"&OK", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer100.Add( self.buttonOK, 0, wx.ALIGN_LEFT|wx.ALL, 5 )
		
		self.buttonBatal = wx.Button( self, wx.ID_CANCEL, u"&Batal", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer100.Add( self.buttonBatal, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		bSizer97.Add( bSizer100, 0, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer97 )
		self.Layout()
		bSizer97.Fit( self )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameDataBank
###########################################################################

class FrameDataBank ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Bank", pos = wx.DefaultPosition, size = wx.Size( 581,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer67 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer68 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer69 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticNoRekening = wx.StaticText( self.panel, wx.ID_ANY, u"No. Rekening", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoRekening.Wrap( -1 )
		bSizer69.Add( self.staticNoRekening, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNoRekening = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer69.Add( self.textNoRekening, 1, wx.ALL, 5 )
		
		self.staticNamaBank = wx.StaticText( self.panel, wx.ID_ANY, u"Nama Bank", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNamaBank.Wrap( -1 )
		bSizer69.Add( self.staticNamaBank, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textNamaBank = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer69.Add( self.textNamaBank, 1, wx.ALL, 5 )
		
		self.btnTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer69.Add( self.btnTambah, 0, wx.ALL, 5 )
		
		bSizer68.Add( bSizer69, 0, wx.EXPAND, 5 )
		
		bSizer32 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSaring = wx.StaticText( self.panel, wx.ID_ANY, u"Saring", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSaring.Wrap( -1 )
		bSizer32.Add( self.staticSaring, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.textSaring = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer32.Add( self.textSaring, 1, wx.ALL, 5 )
		
		bSizer68.Add( bSizer32, 0, wx.EXPAND, 5 )
		
		bSizer70 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer70.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer68.Add( bSizer70, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer68 )
		self.panel.Layout()
		bSizer68.Fit( self.panel )
		bSizer67.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer67 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameTransaksiBank
###########################################################################

class FrameTransaksiBank ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Transaksi Bank", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer76 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer77 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer78 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticBank = wx.StaticText( self.panel, wx.ID_ANY, u"Bank", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticBank.Wrap( -1 )
		bSizer78.Add( self.staticBank, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		comboBankChoices = []
		self.comboBank = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboBankChoices, 0 )
		bSizer78.Add( self.comboBank, 1, wx.ALL, 5 )
		
		bSizer77.Add( bSizer78, 0, wx.EXPAND, 5 )
		
		fgSizer8 = wx.FlexGridSizer( 2, 5, 0, 0 )
		fgSizer8.AddGrowableCol( 1 )
		fgSizer8.AddGrowableRow( 0 )
		fgSizer8.AddGrowableRow( 1 )
		fgSizer8.SetFlexibleDirection( wx.BOTH )
		fgSizer8.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticTanggal = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTanggal.Wrap( -1 )
		fgSizer8.Add( self.staticTanggal, 0, wx.ALL, 5 )
		
		self.staticKeterangan = wx.StaticText( self.panel, wx.ID_ANY, u"Keterangan", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticKeterangan.Wrap( -1 )
		fgSizer8.Add( self.staticKeterangan, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.staticJenis = wx.StaticText( self.panel, wx.ID_ANY, u"Jenis", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticJenis.Wrap( -1 )
		fgSizer8.Add( self.staticJenis, 0, wx.ALL, 5 )
		
		self.staticJumlah = wx.StaticText( self.panel, wx.ID_ANY, u"Jumlah", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticJumlah.Wrap( -1 )
		fgSizer8.Add( self.staticJumlah, 0, wx.ALL, 5 )
		
		
		fgSizer8.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		fgSizer8.Add( self.pickerTanggal, 0, wx.ALL, 5 )
		
		self.textKeterangan = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer8.Add( self.textKeterangan, 1, wx.ALL|wx.EXPAND, 5 )
		
		choiceJenisChoices = []
		self.choiceJenis = wx.Choice( self.panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choiceJenisChoices, 0 )
		self.choiceJenis.SetSelection( 0 )
		fgSizer8.Add( self.choiceJenis, 0, wx.ALL, 5 )
		
		self.numJumlah = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		self.numJumlah.SetEditable(True)
		fgSizer8.Add( self.numJumlah, 0, wx.ALL, 5 )
		
		self.buttonTambah = wx.Button( self.panel, wx.ID_ANY, u"&Tambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer8.Add( self.buttonTambah, 0, wx.ALL, 5 )
		
		bSizer77.Add( fgSizer8, 0, wx.EXPAND, 5 )
		
		bSizer80 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER, sortable=False)
		bSizer80.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer77.Add( bSizer80, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer77 )
		self.panel.Layout()
		bSizer77.Fit( self.panel )
		bSizer76.Add( self.panel, 1, wx.EXPAND, 0 )
		
		self.SetSizer( bSizer76 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameReminderGiro
###########################################################################

class FrameReminderGiro ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Reminder Giro", pos = wx.DefaultPosition, size = wx.Size( 772,300 ), style = wx.CAPTION|wx.CLOSE_BOX|wx.FRAME_FLOAT_ON_PARENT|wx.MAXIMIZE_BOX|wx.MINIMIZE_BOX|wx.SYSTEM_MENU )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer82 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer83 = wx.BoxSizer( wx.VERTICAL )
		
		sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( self.panel, wx.ID_ANY, u"Jatuh Tempo" ), wx.HORIZONTAL )
		
		self.radioButtonSemua = wx.RadioButton( self.panel, wx.ID_ANY, u"Semua", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer1.Add( self.radioButtonSemua, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.radioButtonHari = wx.RadioButton( self.panel, wx.ID_ANY, u"Dalam", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer1.Add( self.radioButtonHari, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.spinHari = wx.SpinCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, -366, 366, 7 )
		sbSizer1.Add( self.spinHari, 0, wx.ALL, 5 )
		
		self.staticHari = wx.StaticText( self.panel, wx.ID_ANY, u"Hari", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticHari.Wrap( -1 )
		sbSizer1.Add( self.staticHari, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.radioButtonTanggal = wx.RadioButton( self.panel, wx.ID_ANY, u"Sampai Tanggal", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer1.Add( self.radioButtonTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		sbSizer1.Add( self.pickerTanggal, 0, wx.ALL, 5 )
		
		bSizer83.Add( sbSizer1, 0, wx.EXPAND, 5 )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer83.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer83 )
		self.panel.Layout()
		bSizer83.Fit( self.panel )
		bSizer82.Add( self.panel, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer82 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameProsesGiro
###########################################################################

class FrameProsesGiro ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Proses Pembayaran Giro", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer84 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panel16 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer85 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer85.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.m_panel16.SetSizer( bSizer85 )
		self.m_panel16.Layout()
		bSizer85.Fit( self.m_panel16 )
		bSizer84.Add( self.m_panel16, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer84 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameUser
###########################################################################

class FrameUser ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Administrasi User", pos = wx.DefaultPosition, size = wx.Size( 573,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer86 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer87 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer88 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer90 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticUser = wx.StaticText( self.panel, wx.ID_ANY, u"User", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticUser.Wrap( -1 )
		bSizer90.Add( self.staticUser, 0, wx.ALL, 5 )
		
		self.textUser = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer90.Add( self.textUser, 1, wx.ALL, 5 )
		
		self.buttonTambahUser = wx.Button( self.panel, wx.ID_ANY, u"&Tambah User Baru", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer90.Add( self.buttonTambahUser, 0, wx.ALL, 5 )
		
		bSizer88.Add( bSizer90, 0, wx.EXPAND, 5 )
		
		bSizer87.Add( bSizer88, 0, wx.EXPAND, 5 )
		
		bSizer89 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer116 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer117 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticUser = wx.StaticText( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticUser.Wrap( -1 )
		bSizer117.Add( self.staticUser, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer116.Add( bSizer117, 0, wx.EXPAND, 5 )
		
		self.objListUser = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer116.Add( self.objListUser, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer89.Add( bSizer116, 1, wx.EXPAND, 5 )
		
		bSizer118 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticResource = wx.StaticText( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticResource.Wrap( -1 )
		bSizer118.Add( self.staticResource, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.objListResource = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		self.objListResource.SetMinSize( wx.Size( 400,-1 ) )
		
		bSizer118.Add( self.objListResource, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer89.Add( bSizer118, 1, wx.EXPAND, 5 )
		
		sbAction = wx.StaticBoxSizer( wx.StaticBox( self.panel, wx.ID_ANY, u"Action" ), wx.VERTICAL )
		
		self.checkBoxLihat = wx.CheckBox( self.panel, wx.ID_ANY, u"&Lihat", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbAction.Add( self.checkBoxLihat, 0, wx.ALL, 5 )
		
		self.checkBoxTambah = wx.CheckBox( self.panel, wx.ID_ANY, u"T&ambah", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbAction.Add( self.checkBoxTambah, 0, wx.ALL, 5 )
		
		self.checkBoxUbah = wx.CheckBox( self.panel, wx.ID_ANY, u"&Ubah", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbAction.Add( self.checkBoxUbah, 0, wx.ALL, 5 )
		
		self.checkBoxHapus = wx.CheckBox( self.panel, wx.ID_ANY, u"&Hapus", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbAction.Add( self.checkBoxHapus, 0, wx.ALL, 5 )
		
		self.checkBoxCetak = wx.CheckBox( self.panel, wx.ID_ANY, u"&Cetak", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbAction.Add( self.checkBoxCetak, 0, wx.ALL, 5 )
		
		self.checkBoxAdmin = wx.CheckBox( self.panel, wx.ID_ANY, u"&Admin", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbAction.Add( self.checkBoxAdmin, 0, wx.ALL, 5 )		
		
		self.buttonCheckSemua = wx.Button( self.panel, wx.ID_ANY, u"Check &Semua", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbAction.Add( self.buttonCheckSemua, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.buttonUncheckSemua = wx.Button( self.panel, wx.ID_ANY, u"U&ncheck Semua", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbAction.Add( self.buttonUncheckSemua, 0, wx.ALL|wx.EXPAND, 5 )
		
		bSizer89.Add( sbAction, 0, wx.EXPAND, 5 )
		
		bSizer87.Add( bSizer89, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer87 )
		self.panel.Layout()
		bSizer87.Fit( self.panel )
		bSizer86.Add( self.panel, 1, wx.EXPAND |wx.ALL, 0 )
		
		self.SetSizer( bSizer86 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameGiro
###########################################################################

class FrameGiro ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Giro", pos = wx.DefaultPosition, size = wx.Size( 736,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer92 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer93 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer94 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.StaticBank = wx.StaticText( self.panel, wx.ID_ANY, u"Bank", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.StaticBank.Wrap( -1 )
		bSizer94.Add( self.StaticBank, 0, wx.ALL, 5 )
		
		comboBankChoices = []
		self.comboBank = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), comboBankChoices, 0 )
		bSizer94.Add( self.comboBank, 0, wx.ALL, 5 )
		
		self.staticNomorGiro = wx.StaticText( self.panel, wx.ID_ANY, u"Nomor Giro", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNomorGiro.Wrap( -1 )
		bSizer94.Add( self.staticNomorGiro, 0, wx.ALL, 5 )
		
		self.textNomorGiro = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		bSizer94.Add( self.textNomorGiro, 0, wx.ALL, 5 )
		
		bSizer93.Add( bSizer94, 0, wx.EXPAND, 5 )
		
		bSizer961 = wx.BoxSizer( wx.HORIZONTAL )
		
		radioBoxJenisChoices = [ u"Keluar", u"Masuk", u"Semua" ]
		self.radioBoxJenis = wx.RadioBox( self.panel, wx.ID_ANY, u"Jenis", wx.DefaultPosition, wx.DefaultSize, radioBoxJenisChoices, 3, wx.RA_SPECIFY_COLS )
		self.radioBoxJenis.SetSelection( 2 )
		bSizer961.Add( self.radioBoxJenis, 0, wx.ALL, 5 )
		
		radioBoxStatusChoices = [ u"Sudah Clear", u"Belum Clear", u"Semua" ]
		self.radioBoxStatus = wx.RadioBox( self.panel, wx.ID_ANY, u"Status", wx.DefaultPosition, wx.DefaultSize, radioBoxStatusChoices, 3, wx.RA_SPECIFY_COLS )
		self.radioBoxStatus.SetSelection( 2 )
		bSizer961.Add( self.radioBoxStatus, 0, wx.ALL, 5 )
		
		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self.panel, wx.ID_ANY, u"Jatuh Tempo" ), wx.HORIZONTAL )
		
		self.pickerDariTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		sbSizer2.Add( self.pickerDariTanggal, 0, wx.ALL, 5 )
		
		self.checkBoxTanggal = wx.CheckBox( self.panel, wx.ID_ANY, u"s/d", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer2.Add( self.checkBoxTanggal, 0, wx.ALL, 5 )
		
		self.pickerSampaiTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		sbSizer2.Add( self.pickerSampaiTanggal, 0, wx.ALL, 5 )
		
		bSizer961.Add( sbSizer2, 0, wx.EXPAND, 5 )
		
		self.buttonCari = wx.Button( self.panel, wx.ID_ANY, u"&Cari", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer961.Add( self.buttonCari, 0, wx.ALL|wx.EXPAND, 5 )
		
		bSizer93.Add( bSizer961, 0, wx.EXPAND, 5 )
		
		bSizer96 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer96.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer93.Add( bSizer96, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer93 )
		self.panel.Layout()
		bSizer93.Fit( self.panel )
		bSizer92.Add( self.panel, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer92 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FramePenyesuaianStock
###########################################################################

class FramePenyesuaianStock ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Penyesuaian Stock", pos = wx.DefaultPosition, size = wx.Size( 656,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer107 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer108 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer109 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticBarang = wx.StaticText( self.panel, wx.ID_ANY, u"Barang", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticBarang.Wrap( -1 )
		bSizer109.Add( self.staticBarang, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.textKodeBarang = wx.TextCtrl(self.panel, wx.ID_ANY, wx.EmptyString,
										wx.DefaultPosition, wx.DefaultSize,
										wx.TE_PROCESS_ENTER)
		bSizer109.Add(self.textKodeBarang, 1, wx.ALL|wx.EXPAND, 5)
		
		self.staticNamaBarang = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString,
											wx.DefaultPosition, wx.DefaultSize, 0)
		self.staticNamaBarang.Wrap(-1)
		bSizer109.Add(self.staticNamaBarang, 2, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
		
#		comboBarangChoices = []
#		self.comboBarang = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboBarangChoices, 0 )
#		bSizer109.Add( self.comboBarang, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.buttonTampil = wx.Button( self.panel, wx.ID_ANY, u"&Tampil", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer109.Add( self.buttonTampil, 0, wx.ALL, 5 )
		
		bSizer108.Add( bSizer109, 0, wx.EXPAND, 5 )
		
		bSizer115 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer113 = wx.BoxSizer( wx.VERTICAL )
		
		radioBoxJenisTransaksiChoices = [ u"Masuk", u"Keluar" ]
		self.radioBoxJenisTransaksi = wx.RadioBox( self.panel, wx.ID_ANY, 
												u"Jenis Transaksi", 
												wx.DefaultPosition, 
												wx.DefaultSize, 
												radioBoxJenisTransaksiChoices, 
												2, wx.RA_SPECIFY_COLS )
		self.radioBoxJenisTransaksi.SetSelection( 0 )
		bSizer113.Add( self.radioBoxJenisTransaksi, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		bSizer115.Add( bSizer113, 0, wx.EXPAND, 5 )
		
		fgSizer17 = wx.FlexGridSizer( 2, 4, 0, 0 )
		fgSizer17.AddGrowableCol( 0 )
		fgSizer17.AddGrowableCol( 1 )
		fgSizer17.AddGrowableRow( 1 )
		fgSizer17.SetFlexibleDirection( wx.BOTH )
		fgSizer17.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticTanggal = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTanggal.Wrap( -1 )
		fgSizer17.Add( self.staticTanggal, 0, wx.ALL, 5 )
		
		self.staticKeterangan = wx.StaticText( self.panel, wx.ID_ANY, u"Keterangan", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticKeterangan.Wrap( -1 )
		fgSizer17.Add( self.staticKeterangan, 0, wx.ALL, 5 )
		
		self.staticHarga = wx.StaticText( self.panel, wx.ID_ANY, u"Harga", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticHarga.Wrap( -1 )
		fgSizer17.Add( self.staticHarga, 0, wx.ALL, 5 )
		
		self.staticQty = wx.StaticText( self.panel, wx.ID_ANY, u"Qty", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticQty.Wrap( -1 )
		fgSizer17.Add( self.staticQty, 0, wx.ALL, 5 )
		
		self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		fgSizer17.Add( self.pickerTanggal, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.textKeterangan = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer17.Add( self.textKeterangan, 2, wx.ALL|wx.EXPAND, 5 )
		
		self.numHarga = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		fgSizer17.Add( self.numHarga, 0, wx.ALL, 5 )
		
		self.numQty = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		fgSizer17.Add( self.numQty, 0, wx.ALL, 5 )
		
		bSizer115.Add( fgSizer17, 1, wx.EXPAND, 5 )
		
		bSizer116 = wx.BoxSizer( wx.VERTICAL )
		
		self.buttonSimpan = wx.Button( self.panel, wx.ID_ANY, u"&Simpan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer116.Add( self.buttonSimpan, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer115.Add( bSizer116, 0, wx.EXPAND, 5 )
		
		bSizer108.Add( bSizer115, 0, wx.EXPAND, 5 )
		
		bSizer110 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer110.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer108.Add( bSizer110, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer108 )
		self.panel.Layout()
		bSizer108.Fit( self.panel )
		bSizer107.Add( self.panel, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer107 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameUtama
###########################################################################

class FrameUtama ( wx.aui.AuiMDIParentFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIParentFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Aplikasi Bisnis", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.CLIP_CHILDREN )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		self.m_menubar1 = wx.MenuBar( 0 )
		self.menuData = wx.Menu()
		self.menuItemBarang = wx.MenuItem( self.menuData, wx.ID_ANY, u"&Barang", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuData.AppendItem( self.menuItemBarang )
		
		self.menuItemCustomer = wx.MenuItem( self.menuData, wx.ID_ANY, u"&Customer", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuData.AppendItem( self.menuItemCustomer )
		
		self.menuItemSupplier = wx.MenuItem( self.menuData, wx.ID_ANY, u"&Supplier", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuData.AppendItem( self.menuItemSupplier )
				
		
		self.menuItemSatuan = wx.MenuItem( self.menuData, wx.ID_ANY, u"Sat&uan", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuData.AppendItem( self.menuItemSatuan )
		
		self.menuItemKategori = wx.MenuItem( self.menuData, wx.ID_ANY, u'&Kategori', wx.EmptyString, wx.ITEM_NORMAL)
		self.menuData.AppendItem( self.menuItemKategori )

		self.menuItemHargaJual = wx.MenuItem( self.menuData, wx.ID_ANY, u"Harga &Jual", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuData.AppendItem( self.menuItemHargaJual )
	
#		self.menuItemBank = wx.MenuItem( self.menuData, wx.ID_ANY, u"Ban&k", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuData.AppendItem( self.menuItemBank )
		
		self.menuItemPenyesuaianStock = wx.MenuItem( self.menuData, wx.ID_ANY, u"&Penyesuaian Stock", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuData.AppendItem( self.menuItemPenyesuaianStock )
		
		self.m_menubar1.Append( self.menuData, u"&Data" )
		
		self.menuSupplier = wx.Menu()
		self.menuItemPembelian = wx.MenuItem( self.menuSupplier, wx.ID_ANY, u"&Pembelian", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuSupplier.AppendItem( self.menuItemPembelian )
		
		self.menuItemReturPembelian = wx.MenuItem( self.menuSupplier, wx.ID_ANY, u"&Retur Pembelian", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuSupplier.AppendItem( self.menuItemReturPembelian )
		
#		self.menuItemPembayaranSupplier = wx.MenuItem( self.menuSupplier, wx.ID_ANY, u"Pem&bayaran Tunai", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuSupplier.AppendItem( self.menuItemPembayaranSupplier )
#		
#		self.menuItemPembayaranGiroSupplier = wx.MenuItem( self.menuSupplier, wx.ID_ANY, u"Pembayaran &Giro", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuSupplier.AppendItem( self.menuItemPembayaranGiroSupplier )
		
		self.m_menubar1.Append( self.menuSupplier, u"&Supplier" )
		
		self.menuCustomer = wx.Menu()
		self.menuItemPenjualan = wx.MenuItem( self.menuCustomer, wx.ID_ANY, u"&Penjualan", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuCustomer.AppendItem( self.menuItemPenjualan )
		
		self.menuItemReturPenjualan = wx.MenuItem( self.menuCustomer, wx.ID_ANY, u"&Retur Penjualan", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuCustomer.AppendItem( self.menuItemReturPenjualan )
		
#		self.menuItemPembayaranCustomer = wx.MenuItem( self.menuCustomer, wx.ID_ANY, u"Pem&bayaran Tunai", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuCustomer.AppendItem( self.menuItemPembayaranCustomer )
#		
#		self.menuItemPembayaranGiroCustomer = wx.MenuItem( self.menuCustomer, wx.ID_ANY, u"Pembayaran &Giro", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuCustomer.AppendItem( self.menuItemPembayaranGiroCustomer )
		
		self.m_menubar1.Append( self.menuCustomer, u"&Customer" )
		
#		self.menuBank = wx.Menu()
#		self.menuItemTransaksiBank = wx.MenuItem( self.menuBank, wx.ID_ANY, u"&Transaksi", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuBank.AppendItem( self.menuItemTransaksiBank )
#		
#		self.menuItemReminderGiro = wx.MenuItem( self.menuBank, wx.ID_ANY, u"&Reminder Giro", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuBank.AppendItem( self.menuItemReminderGiro )
#		
#		self.menuItemGiro = wx.MenuItem( self.menuBank, wx.ID_ANY, u"&Giro", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuBank.AppendItem( self.menuItemGiro )
		
#		self.m_menubar1.Append( self.menuBank, u"&Bank" )
		
		self.menuLaporan = wx.Menu()
#		self.menuItemLaporanStock = wx.MenuItem( self.menuLaporan, wx.ID_ANY, u"&Stock", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuLaporan.AppendItem( self.menuItemLaporanStock )
		self.menuItemLaporanTransaksiStock = wx.MenuItem(self.menuLaporan, 
														wx.ID_ANY,
														u'&Transaksi Stock',
														wx.EmptyString,
														wx.ITEM_NORMAL)
		self.menuLaporan.AppendItem(self.menuItemLaporanTransaksiStock)
		
		self.menuItemLaporanPenjualan = wx.MenuItem( self.menuLaporan, wx.ID_ANY, u"Pen&jualan", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuLaporan.AppendItem( self.menuItemLaporanPenjualan )
		
		self.menuItemLaporanPembelian = wx.MenuItem( self.menuLaporan, wx.ID_ANY, u"Pem&belian", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuLaporan.AppendItem( self.menuItemLaporanPembelian )
		
		self.menuItemLaporanReturPenjualan = wx.MenuItem( self.menuLaporan, wx.ID_ANY, u"&Retur Penjualan", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuLaporan.AppendItem( self.menuItemLaporanReturPenjualan )
		
		self.menuItemLaporanReturPembelian = wx.MenuItem( self.menuLaporan, wx.ID_ANY, u"&Retur Pembelian", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuLaporan.AppendItem( self.menuItemLaporanReturPembelian )
		
#		self.menuItemLaporanPembayaranSupplier = wx.MenuItem( self.menuLaporan, wx.ID_ANY, u"Pembayaran &Supplier", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuLaporan.AppendItem( self.menuItemLaporanPembayaranSupplier )
#		
#		self.menuItemLaporanPembayaranCustomer = wx.MenuItem( self.menuLaporan, wx.ID_ANY, u"Pembayaran &Customer", wx.EmptyString, wx.ITEM_NORMAL )
#		self.menuLaporan.AppendItem( self.menuItemLaporanPembayaranCustomer )
		
		self.m_menubar1.Append( self.menuLaporan, u"&Laporan" )
		
		self.menuUser = wx.Menu()
		self.menuItemAdministrasiPengguna = wx.MenuItem( self.menuUser, wx.ID_ANY, u"&Administrasi Pengguna", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuUser.AppendItem( self.menuItemAdministrasiPengguna )
		
		self.menuItemUbahPassword = wx.MenuItem( self.menuUser, wx.ID_ANY, u"Ubah &Password", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuUser.AppendItem( self.menuItemUbahPassword )
		
		self.menuItemLogout = wx.MenuItem( self.menuUser, wx.ID_ANY, u"&Logout", wx.EmptyString, wx.ITEM_NORMAL )
		self.menuUser.AppendItem( self.menuItemLogout )
		
		self.m_menubar1.Append( self.menuUser, u"&User" )
		

		self.formMenu = wx.Menu()
		self.saveMenu = wx.MenuItem( self.formMenu, wx.ID_ANY, 
									u"&Save", wx.EmptyString, wx.ITEM_NORMAL )	   
		self.formMenu.AppendItem(self.saveMenu)
		
		self.m_menubar1.Append(self.formMenu, "&Form")

		
		self.SetMenuBar( self.m_menubar1 )
		
		self.m_statusBar1 = self.CreateStatusBar( 1, wx.ST_SIZEGRIP, wx.ID_ANY )
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameLaporanStock
###########################################################################

class FrameLaporanStock ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Laporan Stock", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameLaporanPenjualan
###########################################################################

class FrameLaporanPenjualan ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Laporan Penjualan", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer113 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer114 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer117 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticCustomer = wx.StaticText( self.panel, wx.ID_ANY, u"Customer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticCustomer.Wrap( -1 )
		bSizer117.Add( self.staticCustomer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
#		comboCustomerChoices = []
#		self.comboCustomer = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboCustomerChoices, wx.CB_DROPDOWN )
#		bSizer117.Add( self.comboCustomer, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textKodeCustomer = wx.TextCtrl(self.panel, wx.ID_ANY,
										wx.EmptyString, wx.DefaultPosition,
										wx.DefaultSize, 0)
		bSizer117.Add(self.textKodeCustomer, 1, wx.ALL|wx.EXPAND, 5)

		self.staticNamaCustomer = wx.StaticText(self.panel, wx.ID_ANY,
											wx.EmptyString, wx.DefaultPosition,
											wx.DefaultSize, 0)
		self.staticNamaCustomer.Wrap(-1)
		bSizer117.Add(self.staticNamaCustomer, 2, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
		
		self.staticNoFaktur = wx.StaticText(self.panel, wx.ID_ANY, u'No Faktur',
										wx.DefaultPosition, wx.DefaultSize, 0)
		self.staticNoFaktur.Wrap(-1)
		bSizer117.Add(self.staticNoFaktur, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
		
		self.textNoFaktur = wx.TextCtrl(self.panel, wx.ID_ANY, wx.EmptyString,
									wx.DefaultPosition, wx.DefaultSize, 
									wx.TE_PROCESS_ENTER)
		bSizer117.Add(self.textNoFaktur, 1, wx.EXPAND|wx.ALL, 5)
		
		self.staticPeriode = wx.StaticText( self.panel, wx.ID_ANY, u"Periode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPeriode.Wrap( -1 )
		bSizer117.Add( self.staticPeriode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeMulai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeMulai, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticSampaiDengan = wx.StaticText( self.panel, wx.ID_ANY, u"s/d", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSampaiDengan.Wrap( -1 )
		bSizer117.Add( self.staticSampaiDengan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeSelesai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeSelesai, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
#		radioStatusChoices = [ u"Semua", u"Belum Lunas", u"Lunas" ]
#		self.radioStatus = wx.RadioBox( self.panel, wx.ID_ANY, u"Status", wx.DefaultPosition, wx.DefaultSize, radioStatusChoices, 1, wx.RA_SPECIFY_ROWS )
#		self.radioStatus.SetSelection( 0 )
#		bSizer117.Add( self.radioStatus, 0, wx.ALL, 5 )
		
#		self.buttonCetakLaporan = wx.Button( self.panel, wx.ID_ANY, u"&Cetak Laporan", wx.DefaultPosition, wx.DefaultSize, 0 )
#		bSizer117.Add( self.buttonCetakLaporan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		bSizer114.Add( bSizer117, 0, wx.EXPAND, 5 )
		
		bSizer118 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = GroupListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer118.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer114.Add( bSizer118, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer114 )
		self.panel.Layout()
		bSizer114.Fit( self.panel )
		bSizer113.Add( self.panel, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer113 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameLaporanPembelian
###########################################################################

class FrameLaporanPembelian ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Laporan Pembelian", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer113 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer114 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer117 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSupplier = wx.StaticText( self.panel, wx.ID_ANY, u"Supplier", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSupplier.Wrap( -1 )
		bSizer117.Add( self.staticSupplier, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		comboSupplierChoices = []
		self.comboSupplier = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboSupplierChoices, wx.CB_DROPDOWN )
		bSizer117.Add( self.comboSupplier, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticPeriode = wx.StaticText( self.panel, wx.ID_ANY, u"Periode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPeriode.Wrap( -1 )
		bSizer117.Add( self.staticPeriode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeMulai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeMulai, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticSampaiDengan = wx.StaticText( self.panel, wx.ID_ANY, u"s/d", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSampaiDengan.Wrap( -1 )
		bSizer117.Add( self.staticSampaiDengan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeSelesai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeSelesai, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		radioStatusChoices = [ u"Semua", u"Belum Lunas", u"Lunas" ]
		self.radioStatus = wx.RadioBox( self.panel, wx.ID_ANY, u"Status", wx.DefaultPosition, wx.DefaultSize, radioStatusChoices, 1, wx.RA_SPECIFY_ROWS )
		self.radioStatus.SetSelection( 0 )
		bSizer117.Add( self.radioStatus, 0, wx.ALL, 5 )
		
		self.buttonCetakLaporan = wx.Button( self.panel, wx.ID_ANY, u"&Cetak Laporan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer117.Add( self.buttonCetakLaporan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		bSizer114.Add( bSizer117, 0, wx.EXPAND, 5 )
		
		bSizer118 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = GroupListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer118.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer114.Add( bSizer118, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer114 )
		self.panel.Layout()
		bSizer114.Fit( self.panel )
		bSizer113.Add( self.panel, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer113 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameLaporanReturPenjualan
###########################################################################

class FrameLaporanReturPenjualan ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Laporan Retur Penjualan", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer113 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer114 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer117 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticCustomer = wx.StaticText( self.panel, wx.ID_ANY, u"Customer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticCustomer.Wrap( -1 )
		bSizer117.Add( self.staticCustomer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		comboCustomerChoices = []
		self.comboCustomer = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboCustomerChoices, wx.CB_DROPDOWN )
		bSizer117.Add( self.comboCustomer, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticPeriode = wx.StaticText( self.panel, wx.ID_ANY, u"Periode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPeriode.Wrap( -1 )
		bSizer117.Add( self.staticPeriode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeMulai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeMulai, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticSampaiDengan = wx.StaticText( self.panel, wx.ID_ANY, u"s/d", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSampaiDengan.Wrap( -1 )
		bSizer117.Add( self.staticSampaiDengan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeSelesai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeSelesai, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.buttonCetakLaporan = wx.Button( self.panel, wx.ID_ANY, u"&Cetak Laporan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer117.Add( self.buttonCetakLaporan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		bSizer114.Add( bSizer117, 0, wx.EXPAND, 5 )
		
		bSizer118 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = GroupListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer118.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer114.Add( bSizer118, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer114 )
		self.panel.Layout()
		bSizer114.Fit( self.panel )
		bSizer113.Add( self.panel, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer113 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameLaporanReturPembelian
###########################################################################

class FrameLaporanReturPembelian ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Laporan Retur Pembelian", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer113 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer114 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer117 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSupplier = wx.StaticText( self.panel, wx.ID_ANY, u"Supplier", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSupplier.Wrap( -1 )
		bSizer117.Add( self.staticSupplier, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		comboSupplierChoices = []
		self.comboSupplier = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboSupplierChoices, wx.CB_DROPDOWN )
		bSizer117.Add( self.comboSupplier, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticPeriode = wx.StaticText( self.panel, wx.ID_ANY, u"Periode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPeriode.Wrap( -1 )
		bSizer117.Add( self.staticPeriode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeMulai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeMulai, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticSampaiDengan = wx.StaticText( self.panel, wx.ID_ANY, u"s/d", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSampaiDengan.Wrap( -1 )
		bSizer117.Add( self.staticSampaiDengan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeSelesai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeSelesai, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.buttonCetakLaporan = wx.Button( self.panel, wx.ID_ANY, u"&Cetak Laporan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer117.Add( self.buttonCetakLaporan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		bSizer114.Add( bSizer117, 0, wx.EXPAND, 5 )
		
		bSizer118 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = GroupListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer118.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer114.Add( bSizer118, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer114 )
		self.panel.Layout()
		bSizer114.Fit( self.panel )
		bSizer113.Add( self.panel, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer113 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameLaporanPembayaranSupplier
###########################################################################

class FrameLaporanPembayaranSupplier ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Laporan Pembayaran Supplier", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer113 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer114 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer117 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticSupplier = wx.StaticText( self.panel, wx.ID_ANY, u"Supplier", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSupplier.Wrap( -1 )
		bSizer117.Add( self.staticSupplier, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		comboSupplierChoices = []
		self.comboSupplier = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboSupplierChoices, wx.CB_DROPDOWN )
		bSizer117.Add( self.comboSupplier, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticPeriode = wx.StaticText( self.panel, wx.ID_ANY, u"Periode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPeriode.Wrap( -1 )
		bSizer117.Add( self.staticPeriode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeMulai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeMulai, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticSampaiDengan = wx.StaticText( self.panel, wx.ID_ANY, u"s/d", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSampaiDengan.Wrap( -1 )
		bSizer117.Add( self.staticSampaiDengan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeSelesai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeSelesai, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.buttonCetakLaporan = wx.Button( self.panel, wx.ID_ANY, u"&Cetak Laporan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer117.Add( self.buttonCetakLaporan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		bSizer114.Add( bSizer117, 0, wx.EXPAND, 5 )
		
		bSizer118 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = GroupListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer118.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer114.Add( bSizer118, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer114 )
		self.panel.Layout()
		bSizer114.Fit( self.panel )
		bSizer113.Add( self.panel, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer113 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FrameLaporanPembayaranCustomer
###########################################################################

class FrameLaporanPembayaranCustomer ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Laporan Pembayaran Customer", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer113 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer114 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer117 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticCustomer = wx.StaticText( self.panel, wx.ID_ANY, u"Customer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticCustomer.Wrap( -1 )
		bSizer117.Add( self.staticCustomer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		comboCustomerChoices = []
		self.comboCustomer = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboCustomerChoices, wx.CB_DROPDOWN )
		bSizer117.Add( self.comboCustomer, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticPeriode = wx.StaticText( self.panel, wx.ID_ANY, u"Periode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticPeriode.Wrap( -1 )
		bSizer117.Add( self.staticPeriode, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeMulai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeMulai, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.staticSampaiDengan = wx.StaticText( self.panel, wx.ID_ANY, u"s/d", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticSampaiDengan.Wrap( -1 )
		bSizer117.Add( self.staticSampaiDengan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerPeriodeSelesai = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer117.Add( self.pickerPeriodeSelesai, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.buttonCetakLaporan = wx.Button( self.panel, wx.ID_ANY, u"&Cetak Laporan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer117.Add( self.buttonCetakLaporan, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		bSizer114.Add( bSizer117, 0, wx.EXPAND, 5 )
		
		bSizer118 = wx.BoxSizer( wx.VERTICAL )
		
		self.objListDetail = GroupListView(self.panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer118.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer114.Add( bSizer118, 1, wx.EXPAND, 5 )
		
		self.panel.SetSizer( bSizer114 )
		self.panel.Layout()
		bSizer114.Fit( self.panel )
		bSizer113.Add( self.panel, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer113 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class FramePembayaranGiro
###########################################################################

class FramePembayaranGiro ( wx.aui.AuiMDIChildFrame ):
	
	def __init__( self, parent ):
		wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, title = u"Pembayaran Giro", pos = wx.DefaultPosition, size = wx.Size( 750,121 ), style = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.SYSTEM_MENU|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer137 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer138 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer139 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticNama = wx.StaticText( self.panel, wx.ID_ANY, u"Nama", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.staticNama.Wrap( -1 )
		bSizer139.Add( self.staticNama, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		comboNamaChoices = []
		self.comboNama = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboNamaChoices, 0 )
		bSizer139.Add( self.comboNama, 1, wx.ALL, 5 )
		
		self.staticTanggal = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticTanggal.Wrap( -1 )
		bSizer139.Add( self.staticTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		bSizer139.Add( self.pickerTanggal, 0, wx.ALL, 5 )
		
		bSizer138.Add( bSizer139, 0, wx.EXPAND, 5 )
		
		bSizer142 = wx.BoxSizer( wx.HORIZONTAL )
		
		fgSizer16 = wx.FlexGridSizer( 2, 4, 0, 0 )
		fgSizer16.SetFlexibleDirection( wx.BOTH )
		fgSizer16.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.staticNoGiro = wx.StaticText( self.panel, wx.ID_ANY, u"No. Giro", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticNoGiro.Wrap( -1 )
		fgSizer16.Add( self.staticNoGiro, 0, wx.ALL, 5 )
		
		self.staticBank = wx.StaticText( self.panel, wx.ID_ANY, u"Bank", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticBank.Wrap( -1 )
		fgSizer16.Add( self.staticBank, 0, wx.ALL, 5 )
		
		self.staticJatuhTempo = wx.StaticText( self.panel, wx.ID_ANY, u"Jatuh Tempo", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticJatuhTempo.Wrap( -1 )
		fgSizer16.Add( self.staticJatuhTempo, 0, wx.ALL, 5 )
		
		self.staticJumlah = wx.StaticText( self.panel, wx.ID_ANY, u"Jumlah", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticJumlah.Wrap( -1 )
		fgSizer16.Add( self.staticJumlah, 0, wx.ALL, 5 )
		
		self.textNoGiro = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		fgSizer16.Add( self.textNoGiro, 0, wx.ALL, 5 )
		
		comboBankChoices = []
		self.comboBank = wx.ComboBox( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboBankChoices, 0 )
		fgSizer16.Add( self.comboBank, 0, wx.ALL, 5 )
		
		self.pickerJatuhTempo = wx.DatePickerCtrl( self.panel, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		fgSizer16.Add( self.pickerJatuhTempo, 0, wx.ALL, 5 )
		
		self.numBayar = NumCtrl(self.panel, wx.ID_ANY, integerWidth=20, allowNegative=False,groupDigits=True,groupChar='.',decimalChar=',',selectOnEntry=True)
		fgSizer16.Add( self.numBayar, 0, wx.ALL, 5 )
		
		bSizer142.Add( fgSizer16, 0, wx.EXPAND, 5 )
		
		bSizer143 = wx.BoxSizer( wx.VERTICAL )
		
		self.buttonSimpan = wx.Button( self.panel, wx.ID_ANY, u"&Simpan", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer143.Add( self.buttonSimpan, 1, wx.ALL, 5 )
		
		bSizer142.Add( bSizer143, 0, wx.EXPAND, 5 )
		
		bSizer138.Add( bSizer142, 1, 0, 5 )
		
		self.panel.SetSizer( bSizer138 )
		self.panel.Layout()
		bSizer138.Fit( self.panel )
		bSizer137.Add( self.panel, 1, wx.EXPAND, 5 )
		
		self.SetSizer( bSizer137 )
		self.Layout()
	
	def __del__( self ):
		pass
	

