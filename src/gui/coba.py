import wx
import wx.aui
from wx.lib.masked import NumCtrl
from ObjectListView import ObjectListView
import wx.calendar

class FrameTransaksiPenjualan ( wx.aui.AuiMDIChildFrame ):    
    def __init__( self, parent ):
        wx.aui.AuiMDIChildFrame.__init__  ( self, parent, winid = wx.ID_ANY, 
                                            title = u"Transaksi Penjualan", 
                                            pos = wx.DefaultPosition, 
                                            size = wx.DefaultSize, 
                                            style = wx.DEFAULT_FRAME_STYLE )
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        bSizer1 = wx.BoxSizer( wx.VERTICAL )
        
        self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, 
                               wx.Size( -1,-1 ), wx.TAB_TRAVERSAL )
        bSizer2 = wx.BoxSizer( wx.VERTICAL )
        
        bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

        self.staticTanggal = wx.StaticText( self.panel, wx.ID_ANY, u"Tanggal", 
                                            wx.DefaultPosition, wx.DefaultSize, 
                                            0 )
        self.staticTanggal.Wrap( -1 )
        bSizer3.Add( self.staticTanggal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
        
        self.pickerTanggal = wx.DatePickerCtrl( self.panel, wx.ID_ANY, 
                                                wx.DefaultDateTime, 
                                                wx.DefaultPosition, 
                                                wx.DefaultSize, 
                                                wx.DP_DROPDOWN|wx.NO_BORDER )
        bSizer3.Add( self.pickerTanggal, 1, wx.ALL|wx.EXPAND, 5 )

        self.staticNoFaktur = wx.StaticText( self.panel, wx.ID_ANY, 
                                             u"No Faktur", wx.DefaultPosition, 
                                             wx.DefaultSize, 0 )
        self.staticNoFaktur.Wrap( -1 )
        bSizer3.Add( self.staticNoFaktur, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
        
        self.textNoFaktur = wx.TextCtrl( self.panel, wx.ID_ANY, wx.EmptyString, 
                                        wx.DefaultPosition, 
                                        wx.DefaultSize, wx.NO_BORDER )
        self.textNoFaktur.Disable()
        bSizer3.Add( self.textNoFaktur, 1, wx.ALL|wx.EXPAND, 5 )

        
        self.staticCustomer = wx.StaticText( self.panel, wx.ID_ANY, u"Customer", 
                                            wx.DefaultPosition, wx.DefaultSize, 0 )
        self.staticCustomer.Wrap( -1 )
        self.staticCustomer.SetFont( wx.Font( 20, 70, 90, 92, False, wx.EmptyString ) )
        bSizer3.Add( self.staticCustomer, 6, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )        
                
        bSizer2.Add( bSizer3, 0, wx.EXPAND, 5 )
        
        gbSizer = wx.GridBagSizer(5, 5)
        
        self.textKodeBarang = wx.TextCtrl(self.panel, wx.ID_ANY, wx.EmptyString,
                                          wx.DefaultPosition, 
                                          wx.DefaultSize,
                                          wx.TE_PROCESS_ENTER|wx.NO_BORDER)
        self.textKodeBarang.SetFont( wx.Font( 20, 70, 90, 92, False, wx.EmptyString ) )
        gbSizer.Add(self.textKodeBarang, wx.GBPosition(0, 0),
                    wx.GBSpan(1, 7), wx.ALL|wx.EXPAND, 5)
        
        self.numQty = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,
                                groupDigits=True,groupChar='.',
                                decimalChar=',',
                                selectOnEntry=True,
                                integerWidth=10)
        self.numQty.Disable()
        gbSizer.Add(self.numQty, wx.GBPosition(1, 0), wx.GBSpan(1, 1), 
                    wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        
        self.staticSatuan = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString,
                                          wx.DefaultPosition, 
                                          wx.Size(100, -1),
                                          0)
        gbSizer.Add(self.staticSatuan, wx.GBPosition(1, 1), wx.GBSpan(1, 1),
                    wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        
        self.staticNamaBarang = wx.StaticText(self.panel, wx.ID_ANY, wx.EmptyString,
                                              wx.DefaultPosition, 
                                              wx.Size(500, -1),
                                              0)
        gbSizer.Add(self.staticNamaBarang, wx.GBPosition(1, 2), wx.GBSpan(1, 1),
                    wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        
        self.staticAt = wx.StaticText(self.panel, wx.ID_ANY, u'@',
                                      wx.DefaultPosition, wx.DefaultSize, 0)
        gbSizer.Add(self.staticAt, wx.GBPosition(1, 3), wx.GBSpan(1, 1),
                    wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5)
        
        self.numHarga = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,
                                groupDigits=True,groupChar='.',
                                decimalChar=',',
                                selectOnEntry=True,
                                integerWidth=15)
        self.numHarga.Disable()
        gbSizer.Add(self.numHarga, wx.GBPosition(1, 4), wx.GBSpan(1, 1),
                    wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        
        self.numSubTotal = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,
                                groupDigits=True,groupChar='.',
                                decimalChar=',',
                                selectOnEntry=True,
                                integerWidth=20)
        self.numSubTotal.Disable()
        gbSizer.Add(self.numSubTotal, wx.GBPosition(1, 5), wx.GBSpan(1, 1),
                    wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
                
        bSizer2.Add(gbSizer, 0, wx.EXPAND, 5)
        
        self.objListDetail = ObjectListView(self.panel, wx.ID_ANY, 
                                            style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        bSizer2.Add( self.objListDetail, 1, wx.ALL|wx.EXPAND, 5 )
        
        bSizer3 = wx.BoxSizer(wx.HORIZONTAL)        
        
        
        self.staticTotal = wx.StaticText(self.panel, wx.ID_ANY, u'Total',
                                         wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTotal.Wrap(-1)
        bSizer3.Add(self.staticTotal, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        
        self.numTotal = NumCtrl(self.panel, wx.ID_ANY, allowNegative=False,
                                groupDigits=True,groupChar='.',
                                decimalChar=',',
                                selectOnEntry=True,
                                integerWidth=20)
        self.numTotal.Disable()
        bSizer3.Add( self.numTotal, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )
        
        bSizer2.Add(bSizer3, 0, wx.ALL|wx.ALIGN_RIGHT, 5)
        
        self.panel.SetSizer( bSizer2 )
        self.panel.Layout()
        bSizer2.Fit( self.panel )
        bSizer1.Add( self.panel, 1, wx.EXPAND|wx.FIXED_MINSIZE, 5 )
        
        self.SetSizer( bSizer1 )
        self.Layout()
    
    def __del__( self ):
        pass
    
if __name__ == '__main__':
    app = wx.PySimpleApp()
    frame = wx.aui.AuiMDIParentFrame(None, wx.ID_ANY, 'Point Of Sale')
    child = FrameTransaksiPenjualan(frame)
    app.SetTopWindow(frame)
    frame.Show()
    app.MainLoop()