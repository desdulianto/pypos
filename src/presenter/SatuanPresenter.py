'''
Created on 01 Apr 2010

@author: David
'''

class SatuanPresenter(object):
    def __init__(self, satuans, view, interactor):
        self.satuans = satuans
        self.view = view
        interactor.Install(self, view)
        self.isListening = True
        self.initView()
        view.start()
        
    def loadViewFromModel(self):
        if self.isListening:
            self.isListening = False
            self.refreshSatuanList()
            self.isListening = True
            
    def refreshSatuanList(self):
        self.view.setSatuans(self.satuan)
        
class SatuanInteractor(object):
    def Install(self, presenter, view):
        self.presenter = presenter
        self.view = view
        view.objListDetail.SetEmptyListMsg("Tidak ada Data")