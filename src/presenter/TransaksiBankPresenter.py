'''
Created on 02 Apr 2010

@author: David
'''

class TransaksiBankPresenter(object):
    def __init__(self, transaksiBanks, view, interactor):
        self.transaksiBanks = transaksiBanks
        self.view = view
        interactor.Install(self, view)
        self.isListening = True