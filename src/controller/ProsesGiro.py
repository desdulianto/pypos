'''
Created on 04 Apr 2010

@author: David
'''

from db import pelitajaya as pj
import sqlalchemy as sa
from datetime import date
import wx

class DetailProsesGiro(object):
    def __init__(self):
        self.giro = None
        
    def noGiro(self):
        return self.giro.noGiro
    
    def bank(self):
        return self.giro.bank.display()
    
    def jatuhTempo(self):
        return self.giro.jatuhTempo
    
    def jumlah(self):
        return self.giro.fakturBayar.total()
    
    def clear(self):
        return self.giro.clear

class ProsesGiro(object):
    def __init__(self, event=None):
        tanggal = date.today()
        giros = pj.Giro.query.filter_by(jatuhTempo=tanggal, clear=False).all()
        # proses transaksi giro bank
        for giro in giros:
            # proses giro yang dibayarkan ke supplier saja
            if giro.fakturBayar.row_type != 'bayarbeliheader':
                continue
            bank = giro.bank
            if bank.saldo() < giro.fakturBayar.total():
                msg = wx.MessageDialog(None, "Saldo Bank " + bank.display() + " tidak mencukupi", 
                                       "Proses Pembayaran Giro " + giro.noGiro, wx.OK|wx.ICON_INFORMATION)
                msg.ShowModal()
                continue
            else:
                try:
                    # transaksi bank
                    transaksi = pj.TransaksiBank()
                    transaksi.bank = bank
                    transaksi.tanggal = tanggal
                    transaksi.jenis = "KREDIT"
                    transaksi.keterangan = "(AUTO DARI PROSES PEMBAYARAN GIRO) PEMBAYARAN GIRO %s" % giro.noGiro
                    transaksi.jumlah = giro.fakturBayar.total()
                    
                    giro.clear = True
                    pj.session.commit()
                except Exception, e:
                    print e
                    pj.session.rollback()
                    msg = wx.MessageDialog(None, "Kesalahan pada saat memproses giro " + giro.noGiro,
                                           wx.OK |wx.ICON_INFORMATION)
                    msg.ShowModal()
            