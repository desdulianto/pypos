'''
Created on 11 Mar 2010

@author: David
'''

from gui import gui
from db import pelitajaya as pj
import wx
import sqlalchemy
from hashlib import sha1
from controller import auth


class Login(gui.DialogLogin):
    def __init__(self, parent):
        gui.DialogLogin.__init__(self, parent)
        
        self.__auth = False        
        self.textUser.SetWindowStyle(self.textUser.GetWindowStyle() |wx.TE_PROCESS_ENTER)
        self.textPassword.SetWindowStyle(self.textPassword.GetWindowStyle() | wx.TE_PROCESS_ENTER)
        self.textUser.SetFocus()
        self.buttonLogin.Bind(wx.EVT_BUTTON, self.OnLoginClick)
        self.buttonBatal.Bind(wx.EVT_BUTTON, self.OnBatalClick)
        
        self.textUser.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnter)
        self.textPassword.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnter)
                
    def OnTextEnter(self, event):
        obj = event.GetEventObject()
        if obj is self.textUser and self.textUser.GetValue().strip() != '':
            self.textPassword.SetFocus()
        elif obj is self.textPassword and self.textPassword.GetValue().strip() != '':
            if self.textUser.GetValue().strip() != '':
                loginEvent = wx.PyEvent(self.buttonLogin.Id, wx.EVT_BUTTON.typeId)
                self.buttonLogin.GetEventHandler().ProcessEvent(loginEvent)
            self.buttonLogin.SetFocus()
        
    def OnLoginClick(self, event):
        user = self.textUser.GetValue().strip().upper()
        password = self.textPassword.GetValue()
#        try:
#            u = pj.User.query.filter_by(user=user).one()
#            if u.password == sha1(password).hexdigest():
#                self.__auth = True
#        except sqlalchemy.orm.exc.NoResultFound:
#            pass)
        self.__auth = auth.auth(user, password)
        self.EndModal(self.buttonLogin.GetId())
        
    def OnBatalClick(self, event):
        self.EndModal(self.buttonBatal.GetId())
    
    def getAuth(self):
        return self.__auth
                    
