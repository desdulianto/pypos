'''
Created on 13 Mar 2010

@author: David
'''

from wx.lib.masked import NumCtrl
import wx

def integerFormatString(value, digitGroup='.'):
    if type(value) != type(0) and type(value) != type(0L):
        raise ValueError, "%s is a %s" % (value, type(value))

    # reverse string
    s = str(value)[::-1]
    r = []
    count = 0
    for c in s:
        if count > 0 and count % 3 == 0:
            r.append(digitGroup)
        r.append(c)
        count = count + 1
    return ''.join(r)[::-1]

def dateFormatString(value):
#    return "%02d-%02d-%d" % (value.day, value.month, value.year)
    return "{0:02d}-{1:02d}-{2:4d}".format(value.day, value.month, value.year)

def makeNumCtrlEditor(list, row, item):
    editor = NumCtrl(list, wx.ID_ANY, allowNegative=False, groupDigits=True, groupChar=".", decimalChar=",")
    return editor

class MyComboBox(wx.ComboBox):
    def __init__(self, parent, id, st, pos, size, choices, data):
        wx.ComboBox.__init__(self, parent, id, st, pos, size, choices, data)        
    def GetValue(self):
        return self.GetClientData(self.GetSelection())

def populateComboBox(comboBox, items):
    for item in items:
        comboBox.Append(item.display(), item)