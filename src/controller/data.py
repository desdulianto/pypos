'''
Created on 10 Mar 2010

@author: David
'''

from gui import gui
from db import pelitajaya as pj
import wx
import sqlalchemy

from ObjectListView import ColumnDefn, Filter, ObjectListView as olv, ListCtrlPrinter
import ObjectListView

from util import integerFormatString, makeNumCtrlEditor, MyComboBox, populateComboBox

import auth

def makeComboBoxEditor(list, row, item):
    satuans = pj.Satuan.query.all()
    choices = []
    editor = MyComboBox(list, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                         choices, 0)    
    populateComboBox(editor, satuans)
    #editor.Append("<<Tambah Barang Baru>>", -1)
    return editor

class Supplier(gui.FrameDataSupplier): 
    __nama__ = "DATASUPPLIER"
       
    def __init__(self, parent):
        gui.FrameDataSupplier.__init__(self, parent)
        
        self.objListDetail.SetEmptyListMsg("Tidak ada data")
        
        if auth.user.has_permission("UBAH", Supplier.__nama__):
            self.objListDetail.cellEditMode = olv.CELLEDIT_DOUBLECLICK
            
        if auth.user.has_permission("HAPUS", Supplier.__nama__):
            self.objListDetail.Bind(wx.EVT_KEY_DOWN, self.OnListDetailKeyDown)
            
        def _handleText(evt, searchCtrl=self.textSaring, olv=self.objListDetail):
            self.OnTextSearchCtrl(evt, searchCtrl, olv)
        def _handleCancel(evt, searchCtrl=self.textSaring, olv=self.objListDetail):
            self.OnCancelSearchCtrl(evt, searchCtrl, olv)
        self.textSaring.Bind(wx.EVT_TEXT, _handleText)
        self.textSaring.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, _handleCancel)
        self.objListDetail.SetFilter(Filter.TextSearch(self.objListDetail, 
                                                       self.objListDetail.columns[:2]))
        self.populateListDetail()
        
        self.buttonTambah.Bind(wx.EVT_BUTTON, self.OnTambahClick)
        if not auth.user.has_permission("TAMBAH", Supplier.__nama__):
            self.buttonTambah.Disable()
            
        self.textKode.SetFocus()
        
        # enter as navigation
        self.textKode.Bind(wx.EVT_KEY_DOWN, self.OnKodeKeyDown)
        self.textNama.Bind(wx.EVT_KEY_DOWN, self.OnNamaKeyDown)
        self.textAlamat.Bind(wx.EVT_KEY_DOWN, self.OnAlamatKeyDown)
        self.textNoTelp.Bind(wx.EVT_KEY_DOWN, self.OnNoTelpKeyDown)
        self.textKota.Bind(wx.EVT_KEY_DOWN, self.OnKotaKeyDown)


    def OnKodeKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.textNama.SetFocus()
        else:
            event.Skip()
            
    def OnNamaKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.textAlamat.SetFocus()
        else:
            event.Skip()
            
    def OnAlamatKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.textNoTelp.SetFocus()
        else:
            event.Skip()
            
    def OnNoTelpKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.textKota.SetFocus()
        else:
            event.Skip()
            
    def OnKotaKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.buttonTambah.SetFocus()
        else:
            event.Skip()
        
    def OnTextSearchCtrl(self, event, searchCtrl, olv):
        olv.GetFilter().SetText(searchCtrl.GetValue())
        if isinstance(olv, ObjectListView.GroupListView):
            olv.RebuildGroups()
        else:
            olv.RepopulateList()

    def OnCancelSearchCtrl(self, event, searchCtrl, olv):
        searchCtrl.SetValue("")
        self.OnTextSearchCtrl(event, searchCtrl, olv)
        
    def updateNama(self, model, value):
        model.nama = value.strip().upper()
        pj.session.commit()
        
    def updateAlamat(self, model, value):
        model.alamat = value.strip().upper()
        pj.session.commit()
        
    def updateNoTelp(self, model, value):
        model.noTelp = value.strip().upper()
        pj.session.commit()
    
    def updateKota(self, model, value):
        model.kota = value.strip().upper()
        pj.session.commit()        

    def OnListDetailKeyDown(self, event):
        if event.GetKeyCode() == wx.WXK_DELETE:
            i = self.objListDetail.GetFocusedRow()
            if i < 0:
                return
            hapus = wx.MessageDialog(self, 'Yakin hapus?', 'Hapus', wx.YES_NO|wx.ICON_QUESTION)
            ret = hapus.ShowModal()
            if ret == wx.ID_YES:
                obj = self.objListDetail.GetObjectAt(i)
                if len(obj.pembelian) > 0:
                    msg = wx.MessageDialog(self, "Tidak bisa dihapus\nSupplier ini memiliki transaksi", "Hapus", wx.OK|wx.ICON_EXCLAMATION)
                    msg.ShowModal()
                    msg.Destroy()
                    
                    return                
                self.objListDetail.RemoveObject(obj)
                obj.delete()
                pj.session.commit()                                   
            hapus.Destroy()
        else:
            event.Skip()
        
    def populateListDetail(self):        
        self.objListDetail.SetColumns([
            ColumnDefn("Kode", "left", 150, "kode", isEditable=False),
            ColumnDefn("Nama", "left", 250, "nama", isSpaceFilling=True, valueSetter=self.updateNama),
            ColumnDefn("Alamat", "left", 250, "alamat", valueSetter=self.updateAlamat),
            ColumnDefn("No. Telp", "left", 150, "noTelp", valueSetter=self.updateNoTelp),
            ColumnDefn("Kota", "left", 250, "kota", valueSetter=self.updateKota)
        ])
        suppliers = pj.Supplier.query.all()
        self.objListDetail.SetObjects(suppliers)        
        
    def OnTambahClick(self, event):
        try:
            supplier = pj.Supplier()
            #supplier.id = self.textKode.GetValue().strip().upper()
            supplier.kode = self.textKode.GetValue().strip().upper()
            supplier.nama = self.textNama.GetValue().strip().upper()
            supplier.alamat = self.textAlamat.GetValue().strip().upper()
            supplier.noTelp = self.textNoTelp.GetValue().strip().upper()
            supplier.kota = self.textKota.GetValue().strip().upper()
            
            if supplier.kode == '' or supplier.nama == '':
                msg = wx.MessageDialog(self, 'Kode dan nama supplier harus diisi', 'Tambah data supplier', wx.OK)
                msg.ShowModal()
                if supplier.kode == '': 
                    self.textKode.SetFocus()
                else:
                    self.textNama.SetFocus()
                pj.session.rollback()
                return
                
            pj.session.commit()
            self.objListDetail.AddObject(supplier)
            self.clearInput()
            self.textKode.SetFocus()
        except sqlalchemy.orm.exc.FlushError:
            pj.session.rollback()
            msg = wx.MessageDialog(self, "Kode Supplier sudah terdaftar", "Tambah Data Supplier", wx.OK)
            msg.ShowModal()
            self.textKode.SetFocus()
        except sqlalchemy.exc.IntegrityError:
            pj.session.rollback()
            msg = wx.MessageDialog(self, "Kode Supplier sudah terdaftar", "Tambah Data Supplier", wx.OK)
            msg.ShowModal()
            self.textKode.SetFocus()            
    
    def clearInput(self):
        self.textKode.Clear()
        self.textNama.Clear()
        self.textAlamat.Clear()
        self.textNoTelp.Clear()
        self.textKota.Clear()
        
    def simpan(self):
        self.OnTambahClick(None)
        
class Customer(gui.FrameDataCustomer):
    __nama__ = "DATACUSTOMER"
    
    def __init__(self, parent):
        gui.FrameDataCustomer.__init__(self, parent)
        
        self.objListDetail.SetEmptyListMsg("Tidak ada data") 
             
        if auth.user.has_permission("UBAH", Customer.__nama__): 
            self.objListDetail.cellEditMode = olv.CELLEDIT_DOUBLECLICK
        
        if auth.user.has_permission("HAPUS", Customer.__nama__):
            self.objListDetail.Bind(wx.EVT_KEY_DOWN, self.OnListDetailKeyDown)
            
        def _handleText(evt, searchCtrl=self.textSaring, olv=self.objListDetail):
            self.OnTextSearchCtrl(evt, searchCtrl, olv)
        def _handleCancel(evt, searchCtrl=self.textSaring, olv=self.objListDetail):
            self.OnCancelSearchCtrl(evt, searchCtrl, olv)
        self.textSaring.Bind(wx.EVT_TEXT, _handleText)
        self.textSaring.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, _handleCancel)
        self.objListDetail.SetFilter(Filter.TextSearch(self.objListDetail, 
                                                       self.objListDetail.columns[:2]))
        self.populateListDetail()
        
        self.buttonTambah.Bind(wx.EVT_BUTTON, self.OnTambahClick)
        if not auth.user.has_permission("TAMBAH", Customer.__nama__):
            self.buttonTambah.Disable()
            
        self.textKode.SetFocus()
            
        # enter as navigation
        self.textKode.Bind(wx.EVT_KEY_DOWN, self.OnKodeKeyDown)
        self.textNama.Bind(wx.EVT_KEY_DOWN, self.OnNamaKeyDown)
        self.textAlamat.Bind(wx.EVT_KEY_DOWN, self.OnAlamatKeyDown)
        self.textNoTelp.Bind(wx.EVT_KEY_DOWN, self.OnNoTelpKeyDown)
        self.textKota.Bind(wx.EVT_KEY_DOWN, self.OnKotaKeyDown)

    def OnKodeKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.textNama.SetFocus()
        else:
            event.Skip()
            
    def OnNamaKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.textAlamat.SetFocus()
        else:
            event.Skip()
            
    def OnAlamatKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.textNoTelp.SetFocus()
        else:
            event.Skip()
            
    def OnNoTelpKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.textKota.SetFocus()
        else:
            event.Skip()
            
    def OnKotaKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.buttonTambah.SetFocus()
        else:
            event.Skip()
                
    def OnTextSearchCtrl(self, event, searchCtrl, olv):
        olv.GetFilter().SetText(searchCtrl.GetValue())
        if isinstance(olv, ObjectListView.GroupListView):
            olv.RebuildGroups()
        else:
            olv.RepopulateList()

    def OnCancelSearchCtrl(self, event, searchCtrl, olv):
        searchCtrl.SetValue("")
        self.OnTextSearchCtrl(event, searchCtrl, olv)

    def updateNama(self, model, value):
        model.nama = value.strip().upper()
        pj.session.commit()
        
    def updateAlamat(self, model, value):
        model.alamat = value.strip().upper()
        pj.session.commit()
        
    def updateNoTelp(self, model, value):
        model.noTelp = value.strip().upper()
        pj.session.commit()
    
    def updateKota(self, model, value):
        model.kota = value.strip().upper()
        pj.session.commit()

    def OnListDetailKeyDown(self, event):
        if event.GetKeyCode() == wx.WXK_DELETE:
            i = self.objListDetail.GetFocusedRow()
            if i < 0:
                return
            hapus = wx.MessageDialog(self, 'Yakin hapus?', 'Hapus', wx.YES_NO|wx.ICON_QUESTION)
            ret = hapus.ShowModal()
            if ret == wx.ID_YES:
                obj = self.objListDetail.GetObjectAt(i)
                
                if len(obj.hargaJual) > 0 or len(obj.penjualan) > 0:
                    msg = wx.MessageDialog(self, "Tidak bisa dihapus\nCustomer ini memiliki transaksi", "Hapus", wx.OK|wx.ICON_EXCLAMATION)
                    msg.ShowModal()
                    msg.Destroy()
                    return
                self.objListDetail.RemoveObject(obj)
                obj.delete()
                pj.session.commit()                                   
            hapus.Destroy()
        else:
            event.Skip()
        
    def populateListDetail(self):        
        self.objListDetail.SetColumns([
            ColumnDefn("Kode", "left", 150, "kode", isEditable=False),
            ColumnDefn("Nama", "left", 250, "nama", isSpaceFilling=True, valueSetter=self.updateNama),
            ColumnDefn("Alamat", "left", 250, "alamat", valueSetter=self.updateAlamat),
            ColumnDefn("No. Telp", "left", 150, "noTelp", valueSetter=self.updateNoTelp),
            ColumnDefn("Kota", "left", 250, "kota", valueSetter=self.updateKota)            
        ])
        customers = pj.Customer.query.all()
        self.objListDetail.SetObjects(customers)        
        
    def OnTambahClick(self, event):
        try:
            customer = pj.Customer()
            customer.kode = self.textKode.GetValue().strip().upper()
            customer.nama = self.textNama.GetValue().strip().upper()
            customer.alamat = self.textAlamat.GetValue().strip().upper()
            customer.noTelp = self.textNoTelp.GetValue().strip().upper()
            customer.kota = self.textKota.GetValue().strip().upper()

            if customer.kode == '' or customer.nama == '':
                msg = wx.MessageDialog(self, 'Kode dan nama customer harus diisi', 'Tambah data customer', wx.OK)
                msg.ShowModal()
                if customer.kode == '': 
                    self.textKode.SetFocus()
                else:
                    self.textNama.SetFocus()
                pj.session.rollback()
                return

            pj.session.commit()
            self.objListDetail.AddObject(customer)
            self.clearInput()
            self.textKode.SetFocus()
        except sqlalchemy.orm.exc.FlushError:
            pj.session.rollback()
            msg = wx.MessageDialog(self, "Kode Customer sudah terdaftar", "Tambah Data Customer", wx.OK)
            msg.ShowModal()
            self.textKode.SetFocus()
        except sqlalchemy.exc.IntegrityError:
            pj.session.rollback()
            msg = wx.MessageDialog(self, "Kode Customer sudah terdaftar", "Tambah Data Customer", wx.OK)
            msg.ShowModal()
            self.textKode.SetFocus()
                           
    def clearInput(self):
        self.textKode.Clear()
        self.textNama.Clear()
        self.textAlamat.Clear()
        self.textKota.Clear()

    def simpan(self):
        self.OnTambahClick(None)

        

class Sales(gui.FrameDataSales):
    __nama__ = "DATASALES"
    
    def __init__(self, parent):
        gui.FrameDataSales.__init__(self, parent)
        
        self.objListDetail.SetEmptyListMsg("Tidak ada data")
        
        if auth.user.has_permission("UBAH", Sales.__nama__):
            self.objListDetail.cellEditMode = olv.CELLEDIT_DOUBLECLICK
            
        if auth.user.has_permission("HAPUS", Sales.__nama__):
            self.objListDetail.Bind(wx.EVT_KEY_DOWN, self.OnListDetailKeyDown)
            
        def _handleText(evt, searchCtrl=self.textSaring, olv=self.objListDetail):
            self.OnTextSearchCtrl(evt, searchCtrl, olv)
        def _handleCancel(evt, searchCtrl=self.textSaring, olv=self.objListDetail):
            self.OnCancelSearchCtrl(evt, searchCtrl, olv)
        self.textSaring.Bind(wx.EVT_TEXT, _handleText)
        self.textSaring.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, _handleCancel)
        self.objListDetail.SetFilter(Filter.TextSearch(self.objListDetail, 
                                                       self.objListDetail.columns[:2]))
        self.populateListDetail()
        
        self.buttonTambah.Bind(wx.EVT_BUTTON, self.OnTambahClick)
        if not auth.user.has_permission("TAMBAH", Supplier.__nama__):
            self.buttonTambah.Disable()
        
    def OnTextSearchCtrl(self, event, searchCtrl, olv):
        olv.GetFilter().SetText(searchCtrl.GetValue())
        if isinstance(olv, ObjectListView.GroupListView):
            olv.RebuildGroups()
        else:
            olv.RepopulateList()

    def OnCancelSearchCtrl(self, event, searchCtrl, olv):
        searchCtrl.SetValue("")
        self.OnTextSearchCtrl(event, searchCtrl, olv)

    def updateNama(self, model, value):
        model.nama = value.strip().upper()
        pj.session.commit()

    def OnListDetailKeyDown(self, event):
        if event.GetKeyCode() == 127:
            i = self.objListDetail.GetFocusedRow()
            if i < 0:
                return
            hapus = wx.MessageDialog(self, 'Yakin hapus?', 'Hapus', wx.YES_NO|wx.ICON_QUESTION)
            ret = hapus.ShowModal()
            if ret == wx.ID_YES:
                obj = self.objListDetail.GetObjectAt(i)
                
                if len(obj.penjualan) > 0:
                    msg = wx.MessageDialog(self, "Tidak bisa dihapus\nCustomer ini memiliki transaksi", "Hapus", wx.OK|wx.ICON_EXCLAMATION)
                    return
                
                self.objListDetail.RemoveObject(obj)
                obj.delete()
                pj.session.commit()                                   
            hapus.Destroy()
        else:
            event.Skip()
        
    def populateListDetail(self):        
        self.objListDetail.SetColumns([
            ColumnDefn("Kode", "left", 150, "kode", isEditable=False),
            ColumnDefn("Nama", "left", 350, "nama", isSpaceFilling=True, valueSetter=self.updateNama)            
        ])
        sales = pj.Sales.query.all()
        self.objListDetail.SetObjects(sales)        
        
    def OnTambahClick(self, event):
        try:
            sales = pj.Sales()
#            sales.id = self.textKode.GetValue().strip().upper()
            sales.kode = self.textKode.GetValue().strip().upper()
            sales.nama = self.textNama.GetValue().strip().upper()
            pj.session.commit()
            self.objListDetail.AddObject(sales)
            self.clearInput()
            self.textKode.SetFocus()
        except sqlalchemy.orm.exc.FlushError:
            pj.session.rollback()
            msg = wx.MessageDialog(self, "Kode Sales sudah terdaftar", "Tambah Data Sales", wx.OK)
            msg.ShowModal()
            self.textKode.SetFocus()
        except sqlalchemy.exc.IntegrityError:
            pj.session.rollback()
            msg = wx.MessageDialog(self, "Kode Sales sudah terdaftar", "Tambah Data Sales", wx.OK)
            msg.ShowModal()
            self.textKode.SetFocus()            
                           
    def clearInput(self):
        self.textKode.Clear()
        self.textNama.Clear()
                
class Satuan(gui.FrameDataSatuan):
    __nama__ = "DATASATUAN"
    
    def __init__(self, parent):
        gui.FrameDataSatuan.__init__(self, parent)
        
        self.objListDetail.SetEmptyListMsg("Tidak ada data")
        
        if auth.user.has_permission("UBAH", Satuan.__nama__):
            self.objListDetail.cellEditMode = olv.CELLEDIT_DOUBLECLICK
         
        if auth.user.has_permission("HAPUS", Satuan.__nama__):   
            self.objListDetail.Bind(wx.EVT_KEY_DOWN, self.OnListDetailKeyDown)
            
        def _handleText(evt, searchCtrl=self.textSaring, olv=self.objListDetail):
            self.OnTextSearchCtrl(evt, searchCtrl, olv)
        def _handleCancel(evt, searchCtrl=self.textSaring, olv=self.objListDetail):
            self.OnCancelSearchCtrl(evt, searchCtrl, olv)
        self.textSaring.Bind(wx.EVT_TEXT, _handleText)
        self.textSaring.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, _handleCancel)
        self.objListDetail.SetFilter(Filter.TextSearch(self.objListDetail, 
                                                       self.objListDetail.columns[0:1]))
        self.populateListDetail()
        
        self.buttonTambah.Bind(wx.EVT_BUTTON, self.OnTambahClick)
        
        if not auth.user.has_permission("HAPUS", Satuan.__nama__):
            self.buttonTambah.Disable()
            
        self.textNama.Bind(wx.EVT_KEY_DOWN, self.OnNamaKeyDown)       
        
    def OnNamaKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.buttonTambah.SetFocus()
        else:
            event.Skip()
        
    def updateNama(self, model, value):
        model.nama = value.strip().upper()
        pj.session.commit()

    def OnTextSearchCtrl(self, event, searchCtrl, olv):
        olv.GetFilter().SetText(searchCtrl.GetValue())
        if isinstance(olv, ObjectListView.GroupListView):
            olv.RebuildGroups()
        else:
            olv.RepopulateList()

    def OnCancelSearchCtrl(self, event, searchCtrl, olv):
        searchCtrl.SetValue("")
        self.OnTextSearchCtrl(event, searchCtrl, olv)

    def populateListDetail(self):        
        self.objListDetail.SetColumns([
            ColumnDefn("Nama", "left", 350, "nama", isSpaceFilling=True, 
                       valueSetter=self.updateNama),            
        ])
        satuans = pj.Satuan.query.all()
        self.objListDetail.SetObjects(satuans)
        
    def OnTambahClick(self, event):
        try:
            satuan = pj.Satuan()
            satuan.nama = self.textNama.GetValue().strip().upper()

            if satuan.nama == '':
                msg = wx.MessageDialog(self, 'Satuan harus diisi', 'Tambah data satuan', wx.OK)
                msg.ShowModal()
                self.textNama.SetFocus()
                pj.session.rollback()
                return
            
            pj.session.commit()
            self.objListDetail.AddObject(satuan)
            self.clearInput()
            self.textNama.SetFocus()
        except sqlalchemy.orm.exc.FlushError:
            pj.session.rollback()
            msg = wx.MessageDialog(self, "Satuan sudah terdaftar", "Tambah Data Satuan", wx.OK)
            msg.ShowModal()
            self.textSatuan.SetFocus()

    def OnListDetailKeyDown(self, event):
        if event.GetKeyCode() == 127:
            i = self.objListDetail.GetFocusedRow()
            if i < 0:
                return
            hapus = wx.MessageDialog(self, 'Yakin hapus?', 'Hapus', wx.YES_NO|wx.ICON_QUESTION)
            ret = hapus.ShowModal()
            if ret == wx.ID_YES:
                obj = self.objListDetail.GetObjectAt(i)
                if (len(obj.barang) > 0):
                    msg = wx.MessageDialog(self, "Tidak bisa dihapus\nSatuan ini memiliki transaksi", "Hapus", wx.OK|wx.ICON_EXCLAMATION)
                    msg.ShowModal()
                    return                
                self.objListDetail.RemoveObject(obj)
                obj.delete()
                pj.session.commit()                                   
            hapus.Destroy()
        else:
            event.Skip()
            
    def clearInput(self):
        self.textNama.Clear()
        
    def simpan(self):
        self.OnTambahClick(None)

        
class Kategori(gui.FrameDataKategori):
    __nama__ = "DATAKATEGORI"
    
    def __init__(self, parent):
        gui.FrameDataKategori.__init__(self, parent)
        
        self.objListDetail.SetEmptyListMsg("Tidak ada data")
        
        if auth.user.has_permission("UBAH", Satuan.__nama__):
            self.objListDetail.cellEditMode = olv.CELLEDIT_DOUBLECLICK
         
        if auth.user.has_permission("HAPUS", Satuan.__nama__):   
            self.objListDetail.Bind(wx.EVT_KEY_DOWN, self.OnListDetailKeyDown)
            
        def _handleText(evt, searchCtrl=self.textSaring, olv=self.objListDetail):
            self.OnTextSearchCtrl(evt, searchCtrl, olv)
        def _handleCancel(evt, searchCtrl=self.textSaring, olv=self.objListDetail):
            self.OnCancelSearchCtrl(evt, searchCtrl, olv)
        self.textSaring.Bind(wx.EVT_TEXT, _handleText)
        self.textSaring.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, _handleCancel)
        self.objListDetail.SetFilter(Filter.TextSearch(self.objListDetail, 
                                                       self.objListDetail.columns[0:1]))
        self.populateListDetail()
        
        self.buttonTambah.Bind(wx.EVT_BUTTON, self.OnTambahClick)
        
        if not auth.user.has_permission("HAPUS", Satuan.__nama__):
            self.buttonTambah.Disable()
            
        self.textNama.Bind(wx.EVT_KEY_DOWN, self.OnNamaKeyDown)
        
    def OnNamaKeyDown(self, event):
        keyCode = event.GetKeyCode()
        if keyCode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER):
            self.buttonTambah.SetFocus()
        else:
            event.Skip()
        
    def updateNama(self, model, value):
        model.nama = value.strip().upper()
        pj.session.commit()

    def OnTextSearchCtrl(self, event, searchCtrl, olv):
        olv.GetFilter().SetText(searchCtrl.GetValue())
        if isinstance(olv, ObjectListView.GroupListView):
            olv.RebuildGroups()
        else:
            olv.RepopulateList()

    def OnCancelSearchCtrl(self, event, searchCtrl, olv):
        searchCtrl.SetValue("")
        self.OnTextSearchCtrl(event, searchCtrl, olv)

    def populateListDetail(self):        
        self.objListDetail.SetColumns([
            ColumnDefn("Nama", "left", 350, "nama", isSpaceFilling=True, 
                       valueSetter=self.updateNama),            
        ])
        kategoris = pj.Kategori.query.all()
        self.objListDetail.SetObjects(kategoris)
        
    def OnTambahClick(self, event):
        try:
            kategori = pj.Kategori()
            kategori.nama = self.textNama.GetValue().strip().upper()
            if kategori.nama == '':
                msg = wx.MessageDialog(self, 'Kategori harus diisi', 'Tambah data kategori', wx.OK)
                msg.ShowModal()
                self.textNama.SetFocus()
                pj.session.rollback()
                return

            pj.session.commit()
            self.objListDetail.AddObject(kategori)
            self.clearInput()
            self.textNama.SetFocus()
        except sqlalchemy.orm.exc.FlushError:
            pj.session.rollback()
            msg = wx.MessageDialog(self, "Kategori sudah terdaftar", "Tambah Data Kategori", wx.OK)
            msg.ShowModal()
            self.textSatuan.SetFocus()

    def OnListDetailKeyDown(self, event):
        if event.GetKeyCode() == 127:
            i = self.objListDetail.GetFocusedRow()
            if i < 0:
                return
            hapus = wx.MessageDialog(self, 'Yakin hapus?', 'Hapus', wx.YES_NO|wx.ICON_QUESTION)
            ret = hapus.ShowModal()
            if ret == wx.ID_YES:
                obj = self.objListDetail.GetObjectAt(i)
                if (len(obj.barang) > 0):
                    msg = wx.MessageDialog(self, "Tidak bisa dihapus\nSatuan ini memiliki transaksi", "Hapus", wx.OK|wx.ICON_EXCLAMATION)
                    msg.ShowModal()
                    return                
                self.objListDetail.RemoveObject(obj)
                obj.delete()
                pj.session.commit()                                   
            hapus.Destroy()
        else:
            event.Skip()
            
    def clearInput(self):
        self.textNama.Clear()
        
    def simpan(self):
        self.OnTambahClick(None)
