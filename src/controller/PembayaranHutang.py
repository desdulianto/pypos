'''
Created on 28 Mar 2010

@author: David
'''

class ItemPembayaran(object):
    def __init__(self, supplier=None):
        self.supplier = supplier
        self.faktur = None
        self.bayar = 0
    
    def noFaktur(self):
        return self.faktur.noFaktur
    
    def tanggal(self):
        return self.faktur.tanggal
    
    def total(self):
        return self.faktur.total()
    
    def totalBayar(self):
        return self.faktur.totalBayar()
    
    def sisaBayar(self):
        return self.faktur.sisaBayar()