'''
Created on Mar 5, 2010

@author: david
'''

from gui import framepembelian as frmbeli
from db import pelitajaya as pj
import sqlalchemy as sa
import wx

class Pembelian(object):
    def __init__(self):
        self.frm = frmbeli.FramePembelian(None, -1, "")
        
        self.frm.pickerTanggal.SetValue(wx.DateTime.Today())
        self.frm.textSupplier.SetFocus()
        
        self.frm.buttonBrowse.Bind(wx.EVT_LEFT_DOWN, self.OnBrowseSupplier)
        self.frm.buttonSimpan.Bind(wx.EVT_BUTTON, self.OnSimpan)

#        self.frm.comboSupplier.SetFocus()            
#            
#        self.frm.comboSupplier.Bind(wx.EVT_COMBOBOX, self.OnSupplierSelect)
#        self.frm.comboSupplier.Bind(wx.EVT_TEXT, self.OnSupplierText)
#        
#        self.frm.comboSupplier.Bind(wx.EVT_LEFT_DOWN, self.OnSupplierChoice)        
    
    def OnSimpan(self, event):
        print ('hello')
        print(self.frm.pickerTanggal.GetValue())
#        
#    def OnSupplierSelect(self, event):
#        combo = event.GetEventObject()
#        data = combo.GetClientData(event.GetSelection())
#        print (event.GetString(), data)
#        
#    def OnSupplierText(self, event):
#        combo = event.GetEventObject()
#        #q = sa.
#        suppliers = pj.Supplier.query.filter(pj.Supplier.id.like('%' + event.GetString() + '%'))
#        
#        combo.Clear()
#        
#        for supplier in suppliers:
#            combo.Append(supplier.nama, supplier)
            
    def OnBrowseSupplier(self, event):
        browse = frmbeli.FrameBrowse(self.frm, -1, "")
        browse.listCari.InsertColumn(0, 'Kode Supplier')
        browse.listCari.InsertColumn(1, 'Nama Supplier')
        browse.listCari.SetItemCount(100)
        browse.listCari.OnGetItemText = self.OnGetItemText
        
        browse.Show()
        
#    def OnGetItemText(self, item, col):
#        return '%d, %d' % (item, col)
#            
#    def OnSupplierChoice(self, event):
#        print(event)
        
    def Show(self):
        self.frm.Show()
        
class Coba(object):
    def __init__(self):
        self.frm = frmbeli.FramePembelian1(None, -1, "")
    
    def Show(self):
        self.frm.Show()