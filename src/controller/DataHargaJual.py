'''
Created on 28 Mar 2010

@author: David
'''

from db import pelitajaya as pj
import sqlalchemy

class ItemHargaJual(object):
    def __init__(self):
        self.barang = None
        self.harga = 0
        standar = True
        
    def namaBarang(self):
        return self.barang.display()
    
    def hargaStandar(self):
        return self.barang.hargaJual

class DataHargaJual(object):
    def __init__(self, customer=None):
        self.customer = customer
        self.daftarHargaJual = []
        
        if self.customer is not None:
            # ambil harga yang sudah ada di hargaJual        
            hargaJual = pj.HargaJual.query.filter_by(customer=self.customer).order_by(pj.HargaJual.barang).all()
            barang_ids = []
            for h in hargaJual:
                item = ItemHargaJual()
                item.barang = h.barang
                item.harga = h.harga
                if item.harga == item.barang.hargaJual:
                    item.standar = True
                else:
                    item.standar = False
                self.daftarHargaJual.append(item)
                
                barang_ids.append(h.barang.kode)
                
            # ambil harga yang belum ada di hargaJual
            if len(barang_ids) != 0:
                barangs = pj.Barang.query.filter(~pj.Barang.kode.in_(barang_ids)).all()
            else:
                barangs = pj.Barang.query.all()
            for barang in barangs:
                item = ItemHargaJual()
                item.barang = barang
                item.harga = barang.hargaJual
                item.standar = True
                self.daftarHargaJual.append(item)
                
    def update(self):
        if self.customer is None:
            raise ValueError, "Customer belum diisi"
        
        for item in self.daftarHargaJual:
            barang = item.barang
            try:
                hargaJual = pj.HargaJual.query.filter_by(customer=self.customer,
                                                         barang=barang).one()
            except sqlalchemy.orm.exc.NoResultFound:
                if item.standar:
                    continue
                hargaJual = pj.HargaJual()
                hargaJual.customer = self.customer
                hargaJual.barang = barang
            if hargaJual.harga != item.harga:
                hargaJual.harga = item.harga
            
        pj.session.commit()