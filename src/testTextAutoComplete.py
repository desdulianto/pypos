'''
Created on 28 Mar 2010

@author: David
'''

import wx
from gui.TextCtrlAutoComplete import TextCtrlAutoComplete

class testTextAutoComplete:
    def __init__(self):
        args = dict()
        if 1:
            args["colNames"] = ("col1", "col2")
            args["multiChoices"] = [ ("Zoey","WOW"), ("Alpha", "wxPython"),
                                    ("Ceda","Is"), ("Beta", "fantastic"),
                                    ("zoebob", "!!")]
            args["colFetch"] = 1
        else:
            args["choices"] = ["123", "cs", "cds", "Bob","Marley","Alpha"]

        args["selectCallback"] = self.selectCallback

        self.dynamic_choices = [
                        'aardvark', 'abandon', 'acorn', 'acute', 'adore',
                        'aegis', 'ascertain', 'asteroid',
                        'beautiful', 'bold', 'classic',
                        'daring', 'dazzling', 'debonair', 'definitive',
                        'effective', 'elegant',
                        'http://python.org', 'http://www.google.com',
                        'fabulous', 'fantastic', 'friendly', 'forgiving', 'feature',
                        'sage', 'scarlet', 'scenic', 'seaside', 'showpiece', 'spiffy',
                        'www.wxPython.org', 'www.osafoundation.org'
                        ]


        app = wx.PySimpleApp()
        frm = wx.Frame(None,-1,"Test",style=wx.TAB_TRAVERSAL|wx.DEFAULT_FRAME_STYLE)
        panel = wx.Panel(frm)
        sizer = wx.BoxSizer(wx.VERTICAL)

        self._ctrl = TextCtrlAutoComplete(panel, **args)
        but = wx.Button(panel,label="Set other multi-choice")
        but.Bind(wx.EVT_BUTTON, self.onBtMultiChoice)
        but2 = wx.Button(panel,label="Set other one-colum choice")
        but2.Bind(wx.EVT_BUTTON, self.onBtChangeChoice)
        but3 = wx.Button(panel,label="Set the starting choices")
        but3.Bind(wx.EVT_BUTTON, self.onBtStartChoices)
        but4 = wx.Button(panel,label="Enable dynamic choices")
        but4.Bind(wx.EVT_BUTTON, self.onBtDynamicChoices)

        sizer.Add(but, 0, wx.ADJUST_MINSIZE, 0)
        sizer.Add(but2, 0, wx.ADJUST_MINSIZE, 0)
        sizer.Add(but3, 0, wx.ADJUST_MINSIZE, 0)
        sizer.Add(but4, 0, wx.ADJUST_MINSIZE, 0)
        sizer.Add(self._ctrl, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        panel.SetAutoLayout(True)
        panel.SetSizer(sizer)
        sizer.Fit(panel)
        sizer.SetSizeHints(panel)
        panel.Layout()
        app.SetTopWindow(frm)
        frm.Show()
        but.SetFocus()
        app.MainLoop()

    def onBtChangeChoice(self, event):
        #change the choices
        self._ctrl.SetChoices(["123", "cs", "cds", "Bob","Marley","Alpha"])
        self._ctrl.SetEntryCallback(None)
        self._ctrl.SetMatchFunction(None)

    def onBtMultiChoice(self, event):
        #change the choices
        self._ctrl.SetMultipleChoices( [ ("Test","Hello"), ("Other word","World"),
                                        ("Yes!","it work?") ], colFetch = 1 )
        self._ctrl.SetEntryCallback(None)
        self._ctrl.SetMatchFunction(None)

    def onBtStartChoices(self, event):
        #change the choices
        self._ctrl.SetMultipleChoices( [ ("Zoey","WOW"), ("Alpha", "wxPython"),
                                    ("Ceda","Is"), ("Beta", "fantastic"),
                                    ("zoebob", "!!")], colFetch = 1 )
        self._ctrl.SetEntryCallback(None)
        self._ctrl.SetMatchFunction(None)

    def onBtDynamicChoices(self, event):
        '''
        Demonstrate dynamic adjustment of the auto-complete list, based on what's
        been typed so far:
        '''
        self._ctrl.SetChoices(self.dynamic_choices)
        self._ctrl.SetEntryCallback(self.setDynamicChoices)
        self._ctrl.SetMatchFunction(self.match)


    def match(self, text, choice):
        '''
        Demonstrate "smart" matching feature, by ignoring http:// and www. when doing
        matches.
        '''
        t = text.lower()
        c = choice.lower()
        if c.startswith(t): return True
        if c.startswith(r'http://'): c = c[7:]
        if c.startswith(t): return True
        if c.startswith('www.'): c = c[4:]
        return c.startswith(t)

    def setDynamicChoices(self):
        ctrl = self._ctrl
        text = ctrl.GetValue().lower()
        current_choices = ctrl.GetChoices()
        choices = [choice for choice in self.dynamic_choices if self.match(text, choice)]
        if choices != current_choices:
            ctrl.SetChoices(choices)

    def selectCallback(self, values):
        """ Simply function that receive the row values when the
            user select an item
        """
        print "Select Callback called...:",  values


if __name__ == "__main__":
	testTextAutoComplete()